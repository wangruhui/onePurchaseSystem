<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>${newDetail.dettitle }</title>
    <meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" href="<%=basePath%>static/css/reset.css">
    <link rel="stylesheet" href="<%=basePath%>static/css/css.css">
  </head>
  
  <body>
    <div class="page"> 
	<!--content-->
	<div class="content">
		<div class="detail">
			<div class="detail_title">
				<h1>${newDetail.dettitle }</h1>
				<div class="left"><span class="read">${newDetail.topsource }</span> <span class="read"><fmt:formatDate value="${newDetail.createdate }" pattern="yyyy-MM-dd HH:mm:ss"/></span></div>
				 <div class="right"><span class="read">(${newDetail.readamount})阅读</span></div>
			</div>
			<div class="detail_content">
				${newDetail.detcontent }
			</div>
		</div>	
	</div>
</div>
  </body>
</html>
