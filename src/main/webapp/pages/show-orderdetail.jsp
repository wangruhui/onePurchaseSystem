<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath %>static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>static/h-ui/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>static/h-ui/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>static/h-ui/css/style.css" />
<title>查看订单详情</title>
</head>
<body>
 <c:forEach items="${orderDetailsList}" var="list" > 
<div class="cl pd-20" >
  <img class="avatar size-XL l" src="${list.commpic}">
  <dl style="margin-left:80px;">
    <dt><span class="f-18">${list.commname }</span> <span class="pl-10 f-12">型号：${list.modname }</span></dt>
    <dd class="pt-10 f-12" style="margin-left:0"><span class="pl-10 f-12">价格：${list.modprice }￥</span><span class="pl-10 f-12">购买数量：${list.buynumber }个</span></dd>
  </dl>
</div>
</c:forEach>
<div class="pd-20">
  <table class="table">
    <tbody>
      <tr>
        <th class="text-r" >订单号：</th>
        <td>${orderSummary.ordernumber }</td>
      </tr>
      <tr>
        <th class="text-r" >订单状态：</th>
        <td><c:if test="${requestScope.orderSummary.ordstate  == '02' }">待发货</c:if><c:if test="${requestScope.orderSummary.ordstate  == '03' }">待评价</c:if><c:if test="${requestScope.orderSummary.ordstate  == '04' }">已完成</c:if><c:if test="${requestScope.orderSummary.ordstate  == '05' }">退款中</c:if><c:if test="${requestScope.orderSummary.ordstate  == '06' }">完成退款</c:if></td>
      </tr>
      <tr>
        <th class="text-r" >商品数量：</th>
        <td>共${orderSummary.commoditynumber }件</td>
      </tr>
      <tr>
        <th class="text-r" >商品总额：</th>
        <td>${orderSummary.totalprice }￥</td>
      </tr>
      <tr>
        <th class="text-r" >运费：</th>
        <td>${orderSummary.freight }￥</td>
      </tr>
      <tr>
      <tr>
        <th class="text-r" >订单总额：</th>
        <td><fmt:formatNumber value="${orderSummary.freight+orderSummary.totalprice }" pattern="##.##" minFractionDigits="2" ></fmt:formatNumber>￥</td>
      </tr>
      <tr>
        <th class="text-r" >抵押金：</th>
        <td>${orderSummary.mortgageamount }￥</td>
      </tr>
      <tr>
        <th class="text-r" >实付金额：</th>
        <td>${orderSummary.payamount }￥</td>
      </tr>
      <tr>
        <th class="text-r" >支付类型：</th>
        <td><c:if test="${requestScope.orderSummary.paytype == '01' }">微信支付</c:if><c:if test="${requestScope.orderSummary.paytype == '02' }">支付宝支付</c:if></td>
      </tr>
      <tr>
        <th class="text-r" >支付订单号：</th>
        <td>${orderSummary.payordernumber }</td>
      </tr>
      <tr>
        <th class="text-r" >支付时间：</th>
        <td>${orderSummary.paytime }</td>
      </tr>
      <tr>
        <th class="text-r" >收货人姓名：</th>
        <td>${orderSummary.takename }</td>
      </tr>
      <tr>
        <th class="text-r" >收货人电话：</th>
        <td>${orderSummary.takephone }</td>
      </tr>
      <tr>
        <th class="text-r" >收件人地址：</th>
        <td>${orderSummary.takeaddress }</td>
      </tr>
      <tr>
        <th class="text-r" >收件人邮编：</th>
        <td>${orderSummary.takezipcode }</td>
      </tr>
       <tr>
        <th class="text-r" >快递单号：</th>
        <td>${orderSummary.expressnumber }</td>
      </tr>
       <tr>
        <th class="text-r" >快递公司：</th>
        <td>${orderSummary.expresscompany }</td>
      </tr>
       <tr>
        <th class="text-r" >退款理由：</th>
        <td>${orderSummary.refundreson }</td>
      </tr>
    </tbody>
  </table>
</div>
<script type="text/javascript" src="<%=basePath %>js/jquery.min.js"></script> 
<script type="text/javascript" src="<%=basePath %>static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=basePath %>static/h-ui/js/H-ui.admin.js"></script>
</body>
</html>