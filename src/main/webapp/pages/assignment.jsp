<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<%@include file="../common/css.jsp" %>

<title>权限分配</title>
</head>
<body>
<article class="page-container">
	<div  class="form form-horizontal" id="form-admin-role-add">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">网站权限：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<dl class="permission-list">
					<dt>
						<label>模块</label>
					</dt>
					<dd>
						<dl class="cl permission-list2">
						<c:forEach items="${permissionList }"  var="list">
							<dt>
								<label class="">
									<input type="checkbox"  ${list.parentid } value="${list.id }"  class="checkbox" >
									${list.permissionname }</label>
							</dt>
							</c:forEach>
						</dl>
					</dd>
				</dl>
			</div>
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
			<input type="hidden" id="admin" value="${adminId }">
				<button class="btn btn-success radius" id="admin-role-save" name="admin-role-save"><i class="icon-ok"></i> 确定</button>
			</div>
		</div>
	</div>
</article>
<%@include file="../common/edit-js.jsp" %>
<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
  $(function(){
	  $("button").on("click",function(){
		  var ids = "";
		  $(".permission-list :checkbox").each(function(){
				
				if($(this).is(':checked')){
					ids+=$(this).val()+",";
				}
				
			});
		  
		  if(ids == ""){
			  layer.msg('请选模块!',{time:1000});
			  return false;
		  }else{
			 ids= ids.substring(0,ids.length-1);
			  //alert(ids);
			  $.ajax({
					type:"post",
					url:basePath+"/per/addPermissionByAdmin",
					data:{"ids":ids,"id":$("#admin").val()},
					async:false,
					success:function(data){
						if(data.code=="0"){
							layer.msg(data.msg,{time:1000});
							var index = parent.layer.getFrameIndex(window.name);
							 setTimeout(function(){
					        	 parent.layer.close(index);
				        	 },2000);
				        		 
						}else{
							alert('error');
						}
					}
				});
		  }
		  
	  })
  });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>