<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<%@include file="../common/css.jsp" %>
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<style type="text/css">

</style>
<title>订单管理</title>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 商城管理 <span class="c-gray en">&gt;</span> 订单管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
  <div > 
    订单号: <input type="text" class="input-text" style="width:250px" placeholder="" id="orderNo" name="orderNo">
    订单状态: <span class="select-box inline">
    <select class="select" id="orderStatus" name="orderStatus">
      <option value="">请选择订单状态</option>
      <option value="02">待发货</option>
      <option value="03">待评价</option>
      <option value="04">已完成</option>
    </select>
    </span> 支付时间: <input type="text" onfocus="WdatePicker()" id="payDate" class="input-text Wdate" style="width:120px;">
    订单创建时间: <input type="text" onfocus="WdatePicker()" id="creOrdreDate" class="input-text Wdate" style="width:120px;">
    <button type="submit" class="btn btn-success" id="search" name=""search""><i class="Hui-iconfont">&#xe665;</i> 搜索</button>
  </div>
  <div class="mt-20">
    <table class="table table-border table-bordered table-bg table-hover table-sort">
    <thead>
      <tr class="text-c">
        <th width="25"><input type="checkbox" name="" value=""></th>
        <th width="150">订单号</th>
        <th width="150">订单状态</th>
        <th width="150">购买用户</th>
        <th width="150">商品数量</th>
        <th width="150">下单时间</th>
        <th width="150">商品总额</th>
        <th width="150">运费</th>
        <th width="150">订单总额</th>
        <th width="150">实付金额</th>
        <th width="150">抵押金额</th>
        <th width="150">支付时间</th>
        <th width="150">支付类型</th>
        <th width="100">操作</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <div class='dataTables_paginate paging_simple_numbers'  id='DataTables_Table_0_paginate'></div>
  
</div>
</div>
 <%@include file="../common/js.jsp" %>
<script type="text/javascript" src="<%=basePath%>/static/js/order-list.js"></script>
</body>
</html>
