<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<%@include file="../common/css.jsp" %>

<title>添加权限</title>
</head>
<body>
	<article class="page-container aform">
	    <form class="form form-horizontal" id="form-admin-add" >
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>权限名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" placeholder="" id="permissionName" name="permissionName">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>权限图标：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" placeholder="" id="ioc" name="ioc">
				</div>
			</div>
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>
</body>
</html>
<%@include file="../common/edit-js.jsp" %> 
<script type="text/javascript">
$(document).ready(function(){	
	var id = getQueryString('id');
	var url = basePath+"/per/addPermission";
	
	if(id != null){
		url = basePath+"/per/editPermission?id="+id+"";
		
		$.ajax({
			type:"post",
			url:basePath+"/per/findPermissionById",
			data:{"id":id},
			async:false,
			success:function(data){
				
				$('#permissionName').val(data.data.permissionName);
				$('#ioc').val(data.data.ioc);
			}
		});
		
	}
	
	$(".form-horizontal").validate({
		rules:{
			permissionName:{
				required:true
			},
			ioc:{
				required:true
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
		        	 },1000)
		        	 
		        	
		        }
		      });
		}
	});
	
});

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>