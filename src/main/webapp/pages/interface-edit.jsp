<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="http://lib.h-ui.net/html5.js"></script>
<script type="text/javascript" src="http://lib.h-ui.net/respond.min.js"></script>
<script type="text/javascript" src="http://lib.h-ui.net/PIE_IE678.js"></script>
<![endif]-->
<%@include file="../common/css.jsp" %>

<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>添加界面</title>
</head>
<body>
	<article class="page-container aform">
	    <form class="form form-horizontal" id="form-admin-add" >
			<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>界面类型：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="interfaceName" id="interfaceName" class="select">
				</select>
				</span> </div>
	        </div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>界面链接：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" placeholder="http://" id="interfaceURL" name="interfaceURL">
				</div>
			</div>
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>
</body>
</html>
<%@include file="../common/edit-js.jsp" %> 
<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript">
$(document).ready(function(){	
	var id = getQueryString('id');
	
	var url = basePath+"/basic/addInterface";
	
	$.post(basePath+"/sys/findDictionByTablenameAndTableField",{"tableName":"sta_interface_layout","tableField":"interfacename"},function(data){
		 for(var i = 0; i < data.data.length; i++){
			 $("#interfaceName").append("<option value='"+data.data[i].fieldvalue+"'>"+data.data[i].displayvalue+"</option>");
		 }
	});
	
	if(id != null){
		url = basePath+"/basic/editInterface?id="+id+"";
		
		
		$.ajax({
			type:"post",
			url:basePath+"/basic/findInterfaceById",
			data:{"id":id},
			async:false,
			success:function(data){
				$('#interfaceName').val(data.data.interfacename);
				$('#interfaceURL').val(data.data.interfaceurl);
			}
		});
		
	}
	
	$(".form-horizontal").validate({
		rules:{
			interfaceName:{
				required:true,
			},
			interfaceURL:{
				required:true
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
		        	 },1000)
		        	 
		        	
		        }
		      });
		}
	});
	
});

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>