<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 <!-- Javascript -->
  <script src="static/login/js/jquery-1.8.2.min.js"></script>
  <script src="static/login/js/supersized.3.2.7.min.js"></script>
  <script src="static/login/js/supersized-init.js"></script>
  <script src="static/login/js/scripts.js"></script>
<html lang="cn" class="no-js">

    <head>
        <base href="<%=basePath%>">
        <meta charset="utf-8">
        <title>${sc.systemname }</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel="stylesheet" href="<%=basePath%>/static/login/css/reset.css">
        <link rel="stylesheet" href="<%=basePath%>/static/login/css/supersized.css">
        <link rel="stylesheet" href="<%=basePath%>/static/login/css/style.css">

        

    </head>

    <body>

        <body>

        <div class="page-container">
            <h1 id="sysName"></h1>
            <form action="<%=basePath%>admin/login" method="post">
                <input type="text" name="username" class="username" placeholder="用户名">
                <input type="password" name="password" class="password" placeholder="密码">
                <button type="submit">登录</button>
                <div class="error"><span>+</span></div>
            </form>
            <div style="margin-top: 20px;font-size: 15px;color:red; "><span>${error}</span></div>
        </div>
        <!--<div class="connect" align="center"><span id="footright"></span></div>
        <div class="connect" align="center"><span id="recordnumber"></span></div>  -->

       
    </body>

</html>

