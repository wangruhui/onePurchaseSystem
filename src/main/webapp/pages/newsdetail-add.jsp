<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<%@include file="../common/css.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/css/bootstrap-fileupload.css" />
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>新增资讯</title>
</head>
<body>
<article class="page-container">

	<form class="form form-horizontal" id="form-article-add"  method="post"  enctype="multipart/form-data" >
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>资讯标题：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="" placeholder="" id="dettitle" name="dettitle">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>资讯来源：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="超级女声" placeholder="" id="topsource" name="topsource">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>资讯类型：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="dettype"  id="dettype" class="select">
					<option value="">请选择资讯类型</option>
				</select>
				</span> </div>
		</div>
		<div class="row cl">
		<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>启用：</label>
		<div id="displayStatus" class="formControls col-xs-8 col-sm-9 skin-minimal">
			<div class="radio-box">
				<input name="displaystatus"  value = "0"  type="radio" id="sex-1" checked>
				<label for="sex-1">启用</label>
			</div>
			<div class="radio-box">
				<input type="radio"  value="1" id="sex-2" name="displaystatus">
				<label for="sex-2">停用</label>
			</div>
		</div>
	</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">显示排序值：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="0" placeholder="倒序排序" id="displayorder" name="displayorder"  style="width:200px;">
				<span class="label label-warning radius">提示!</span>		
				<span >倒序排序</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">发布日期：</label>
			<div class="formControls col-xs-8 col-sm-9">
		        <input type="text" onfocus="WdatePicker()" id="createtime"  name="createtime" class="input-text Wdate" style="width:200px;">
			</div>
		</div>
		<!-- Start 上传图片--> 
	    <div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" >资讯图片：</label>
			<div class="formControls col-xs-8 col-sm-9">                          	
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">	</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; ">
							<img id="listPicture" src="" alt="" style="max-width: 200px; max-height: 150px; " />
						</div>		
						<div>
							<span class="btn btn-file"><span class="fileupload-new">选择图片</span>
							<span class="fileupload-exists">修改</span>
							<input type="file" class="default" name="detpicture"  /></span>
							<a href="javascript:void();" id="removePic" class="btn fileupload-exists" data-dismiss="fileupload">移除</a>
							<input id="listPicturet" type="hidden" name="detpicture" value="">
						</div>
					</div>		
					<span class="label label-warning radius">提示!</span>	
					<span>仅支持png,jpg格式的图片( 290*200 )</span>	          
			</div>
		</div>
		 <!-- End 图片上传-->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">资讯内容：</label>
			<div class="formControls col-xs-8 col-sm-9"> 
				<script id="editor" type="text/plain"  name="detcontent" style="width:100%;height:400px;"></script> 
			</div>
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
				<button id="subBtn" class="btn btn-secondary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 提交</button>
				<button onClick="removeIframe();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</article>

<%@include file="../common/edit-js.jsp" %>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="<%=basePath%>/lib/My97DatePicker/WdatePicker.js"></script>  
<script type="text/javascript" src="<%=basePath%>/lib/webuploader/0.1.5/webuploader.min.js"></script> 
<script type="text/javascript" src="<%=basePath%>/lib/ueditor/ueditor.config.js"></script> 
<script type="text/javascript" src="<%=basePath%>/lib/ueditor/ueditor.all.js"> </script> 
<script type="text/javascript" src="<%=basePath%>static/js/jquery.form.min.js"></script>
<!-- 引入文件上传插件-->
<script type="text/javascript" src="<%=basePath%>/static/js/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/newsdetail-add.js"></script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>
