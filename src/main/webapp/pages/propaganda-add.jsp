<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<%@include file="../common/css.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/css/bootstrap-fileupload.css" />
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>新增资讯</title>
</head>
<body>
<article class="page-container">
	<form class="form form-horizontal" id="form-article-add"  method="post"  enctype="multipart/form-data" >
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>宣传位置：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="picposition"  id="picposition" class="select">
					<option value="">请选择宣传位置</option>
					<option value="1">启动页图片</option>
					<option value="2">首页轮播图片</option>
				</select>
				</span> </div>
		</div>
		<div class="row cl">
		<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>启用：</label>
		<div id="displayStatus" class="formControls col-xs-8 col-sm-9 skin-minimal">
			<div class="radio-box">
				<input name="displaystatus"  value = "0"  type="radio" id="sex-1" checked>
				<label for="sex-1">启用</label>
			</div>
			<div class="radio-box">
				<input type="radio"  value="1" id="sex-2" name="displaystatus">
				<label for="sex-2">停用</label>
			</div>
		</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">显示排序值：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="0" placeholder="" id="displayorder" name="displayorder">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>跳转类型：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="operationevent"  id="operationevent" class="select">
					<option value="">请选择跳转类型</option>
					<option value="1">H5跳转</option>
					<option value="2">话题跳转</option>
					<option value="3">资讯跳转</option>
				</select>
				</span> </div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>跳转类型：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
				<select name="eventinfo"  id="eventinfo" class="select">
				</select>
				</span> </div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>H5跳转地址：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="" placeholder="" id="h5event" name="h5event">
			</div>
		</div>
		<!-- Start 上传图片--> 
	    <div class="row cl">
			<label class="form-label col-xs-4 col-sm-3" ><span class="c-red">*</span>宣传图片：</label>
			<div class="formControls col-xs-8 col-sm-9">                          	
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">	</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; ">
							<img id="listPicture" src="" alt="" style="max-width: 200px; max-height: 150px; " />
						</div>		
						<div>
							<span class="btn btn-file"><span class="fileupload-new">选择图片</span>
							<span class="fileupload-exists">修改</span>
							<input type="file" class="default" name="picaddress" id="pictrure" /></span>
							<a href="javascript:void();" id="removePic" class="btn fileupload-exists" data-dismiss="fileupload">移除</a>
							<input id="listPicturet" type="hidden" name="picaddress" value="">
						</div>
					</div>		
					<span class="label label-important">提示!</span>		
					<span id = "alert">仅支持png,jpg格式的图片</span>	          
			</div>
		</div>
		 <!-- End 图片上传-->
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<button id="subBtn" class="btn btn-secondary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 提交</button>
				<button onClick="removeIframe();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</article>

<%@include file="../common/edit-js.jsp" %>

<script type="text/javascript" src="<%=basePath%>/static/js/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/propaganda-add.js"></script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>
