<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<%@include file="../common/css.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/css/bootstrap-fileupload.css" />
<style type="text/css">
   .progress{
   margin-left:auto;margin-right:auto;
    z-index: auto;
     position: absolute;
     text-align: center;
    width: 100%;
    height: 100%;
    display: none;
    left: 0;
    top: 0;
    background:black;
    filter:alpha(opacity=50);opacity:0.5;
   }
   .proimg{
       position: absolute;
	    left: 50%;
	    top: 50%;
	    transform: translateY(-50%);
   }
</style>

<title>关键词</title>
</head>
<body>
<article class="page-container">
	<form class="form form-horizontal" id="form-article-add"  method="post"  enctype="multipart/form-data"  >
	<!-- Start 上传图片--> 
    <div class="row cl">
		<label class="form-label col-xs-4 col-sm-3" >Excel文件：</label>
		<div class="formControls col-xs-8 col-sm-9">                          	
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" style="width: 200px; height: 30px;">	</div>
					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; ">
						<img id="listPicture" src="" alt="" style="max-width: 200px; max-height: 150px; " />
					</div>		
					<div>
						<span class="btn btn-file"><span class="fileupload-new">选择文件</span>
						<span class="fileupload-exists">修改</span>
						<input type="file" class="default" name="file" id="file" /></span>
						<a href="javascript:void();" id="removePic" class="btn fileupload-exists" data-dismiss="fileupload">移除</a>
						<input id="listPicturet" type="hidden" name="picaddress" value="">
					</div>
				</div>		
				<span class="label label-important">提示!</span>		
				<span>仅支持excel格式</span>	          
		</div>
	 </div>
	 <!-- End 图片上传-->
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<button id="subBtn" class="btn btn-secondary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 提交</button>
				<button onClick="removeIframe();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
	</form>
</article>
<div class="progress">
   <img class="proimg" alt="" src="<%=basePath%>images/progress.gif">
</div>
<%@include file="../common/edit-js.jsp" %>

<script type="text/javascript" src="<%=basePath%>/static/js/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<%=basePath%>/static/js/keyword-excel.js"></script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>
