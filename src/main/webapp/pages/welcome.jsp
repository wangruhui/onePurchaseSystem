<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div class="page-container">
	<p class="f-20 text-success">后台管理系统</p>
	<p>登录次数：${loginnum} </p>
	<p>上次登录IP：${lastLoginIp}  上次登录时间：${lastLoginDate}</p>
	<div style="width:700px;border:1px solid #ddd;">
			<div class="cl pd-20" style=" background-color:#5bacb6">
			  <img class="round avatar size-XL l" src="<c:if test='${admin.headportrait ==null}'><%=basePath %>static/h-ui/images/user.png</c:if><c:if test='${admin.headportrait !=null}'><%=basePath %>${admin.headportrait}</c:if>" >
			  <dl style="margin-left:80px; color:#fff">
			    <dt><span class="f-18">${admin.name }</span></dt>
			    <dd class="pt-10 f-12" style="margin-left:0"><c:if test="${admin.description == null || admin.description == ''}">你什么都没有留下！！！！</c:if><c:if test="${admin.description != null || admin.description != ''}">${admin.description}</c:if></dd>
			  </dl>
		</div>
		<div class="pd-20">
		  <table class="table">
		    <tbody>
		      <tr>
		        <th class="text-r" >管理员类型：</th>
		        <td>${admin.type }</td>
		      </tr>
		      <tr>
		        <th class="text-r" width="80">性别：</th>
		        <td>${admin.gender }</td>
		      </tr>
		      <tr>
		        <th class="text-r">手机：</th>
		        <td>${admin.phone}</td>
		      </tr>
		      <tr>
		        <th class="text-r">邮箱：</th>
		        <td>${admin.email}</td>
		      </tr>
		      <tr>
		        <th class="text-r">操作权限：</th>
		        <td><c:forEach items="${getPermissionList}" var ="list"  varStatus="stat">${list.permissionName } <c:if test="${!stat.last}">、</c:if></c:forEach></td>
		      </tr>
		    </tbody>
		  </table>
		</div>
	</div>
	
	<table class="table table-border table-bordered table-bg mt-20">
		<thead>
			<tr>
				<th colspan="2" scope="col">服务器信息</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th width="30%">服务器计算机名</th>
				<td><span id="lbServerName">${useName }</span></td>
			</tr>
			<tr>
				<td>服务器IP地址</td>
				<td>${addr }</td>
			</tr>
			<tr>
				<td>服务器域名</td>
				<td>${serverName }</td>
			</tr>
			<tr>
				<td>服务器端口 </td>
				<td>${port}</td>
			</tr>
			<tr>
				<td>服务器操作系统 </td>
				<td>${osName }</td>
			</tr>
			<tr>
				<td>系统所在文件夹 </td>
				<td>${filePath }</td>
			</tr>
			<tr>
				<td>服务器上次启动到现在已运行 </td>
				<td>${ functionTime}分钟</td>
			</tr>
			<tr>
				<td>CPU 总数 </td>
				<td>${cpu }</td>
			</tr>
		</tbody>
	</table>
</div>
<footer class="footer mt-20">
	<div class="container">
		<p>感谢我<br>
			Copyright &copy;2015 YZZ All Rights Reserved.<br>
			本后台系统由天才开发</p>
	</div>