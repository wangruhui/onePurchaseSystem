
var url = basePath+"/basic/addKeyword";
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	 //alert($('#newsrefresh').attr('href'));
	$list = $("#fileList"),
	$btn = $("#btn-star"),
	$.ajaxSetup({   
        async : false  
    }); 
	
	var id = getQueryString('id');
	
	if(id != null ){
		$.post(basePath+"/basic/getKeyWordById",{"id":id},function(data){
			 $('#picposition').val(data.data.picposition);
			 url = basePath+"/basic/editKeyword?id="+id;
			 
			 var displaystatusHtml = ""; 
			 if(data.data.enable == "0"){
				 displaystatusHtml = '<div class="radio-box">'+
													'<input name="enable"  value = "0"  type="radio" id="sex-1" checked>'+
													'<label for="sex-1">启用</label>'+
												'</div>'+
													'<div class="radio-box">'+
													'<input type="radio"  value="1" id="sex-2" name="enable">'+
													'<label for="sex-2">停用</label>'+
												'</div>';
			 }else{
				 displaystatusHtml = '<div class="radio-box">'+
													'<input name="enable"  value = "0"  type="radio" id="sex-1" >'+
													'<label for="sex-1">启用</label>'+
												'</div>'+
													'<div class="radio-box">'+
													'<input type="radio"  value="1" id="sex-2" name="enable" checked>'+
													'<label for="sex-2">停用</label>'+
												'</div>';
			 }
			 $('#displayStatus').html(displaystatusHtml);
			 $('#word').val(data.data.word);
		});
	}
	
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	
	
	fromValidate($('.form'));
	
	
});



var fromValidate = function(form){

	
	
	$(form).validate({
		rules:{
			word:{
				required:true,
				maxlength:16
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 