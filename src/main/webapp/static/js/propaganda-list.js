var curPage = 1;
$(function(){
	
	getPropagandaList(curPage);
	//分页操作
	$("body").on("click",".paginate_button",function(){
		getPropagandaList($(this).data('page'));
	});
});

var getPropagandaList = function(curPage){
	var html="";
	$.post(basePath+"/basic/getPropagandaList",{"curPage":curPage},function(data){
		for(var i = 0; i < data.data.length; i++){
			
			var isenable='<span class="label radius">已停用</span>';
			var bBtn = '<a style="text-decoration:none" onclick="propaganda_start(this,\''+data.data[i].id+'\')" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe615;</i></a>';
			if(data.data[i].displaystatus == "0"){
				isenable = '<span class="label label-success radius">已启用</span>';
				bBtn = '<a style="text-decoration:none" onclick="propaganda_stop(this,\''+data.data[i].id+'\')" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a>';
			}
			
			html+='<tr class="text-c">'+
			'<td><input type="checkbox" value="1" data-id=\''+data.data[i].id+'\'></td>'+
			'<td>'+data.data[i].picposition+'</td>'+
			'<td><img style="width:50px;height:50px;" src="'+basePath+'/'+data.data[i].picaddress+'"  alt="" /></td>'+
			'<td>'+data.data[i].operationevent+'</td>'+
			'<td>'+data.data[i].eventinfo+'</td>'+
			'<td>'+data.data[i].createname+'</td>'+
			'<td>'+data.data[i].updatename+'</td>'+
			'<td>'+data.data[i].updatedate+'</td>'+
			'<td>'+data.data[i].displayorder+'</td>'+
			'<td class="td-status">'+isenable+'</td>'+
			'<td class="td-manage">'+bBtn+'<a title="编辑" href="javascript:;" onclick="propaganda_add(\'编辑宣传\',\''+basePath+'/propagandaEdit?id='+data.data[i].id+'\',\'800\',\'800\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="propaganda_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>'+
		'</tr>';
		}
		
		if(html == ""){
			html = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">没有数据</td></tr>';
		}
		
		$('tbody').html(html);
		$('#DataTables_Table_0_paginate').html(data.pages);
		
	});
	
	
}

/*宣传-增加*/
function propaganda_add(title,url,w,h){
	layer_show(title,url,w,h);
}

/*宣传-删除*/
function propaganda_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		
		$.ajax({
			type:"post",
			url:basePath+"/basic/delPropaganda",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}


/*管理员-停用*/
function propaganda_stop(obj,id){
	layer.confirm('确认要停用吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/basic/handleDisplay",
			data:{"id":id,"status":"1"},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a onClick="propaganda_start(this,\''+id+'\')" href="javascript:;" title="启用" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">已禁用</span>');
					$(obj).remove();
					layer.msg('已停用!',{icon: 5,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}

/*管理员-启用*/
function propaganda_start(obj,id){
	layer.confirm('确认要启用吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/basic/handleDisplay",
			data:{"id":id,"status":"0"},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a onClick="propaganda_stop(this,\''+id+'\')" href="javascript:;" title="停用" style="text-decoration:none"><i class="Hui-iconfont">&#xe631;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
					$(obj).remove();
					layer.msg('已启用!', {icon: 6,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/basic/delPropaganda",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getPropagandaList(curPage);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}