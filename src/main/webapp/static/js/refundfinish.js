
var url = "";
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	 
	
	var id = getQueryString('id');
	url = basePath+"/order/refundFinish?id="+id;
	//事件跳转
	$("#outType").change(function(){
		if($(this).children('option:selected').val() == "03"){
			$("#accountName").attr("disabled",false);
			$("#bankAccount").attr("disabled",false);
			$("#cardNumber").attr("disabled",false);
		}else{
			$("#accountName").attr("disabled",true);
			$("#bankAccount").attr("disabled",true);
			$("#cardNumber").attr("disabled",true);
			$("#accountName").val("");
			$("#bankAccount").val("");
			$("#cardNumber").val("");
			
		}
	});
	
	
	
	
	
	
	
	//验证
	$('#subBtn').on('click',function(){
		//alert($('#dettype').val());
		if($('#outType').val() == ""){
			layer.msg("请选择退款类型",{time:1000});
			return false;
		}
		
	});
	
	fromValidate($('#refund-form'));
	
});



var fromValidate = function(form){

	
	
	$(form).validate({
		rules:{
			payorderId:{
				required:true
			},
			outaMount:{
				required:true
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 