
var url = basePath+"/news/addNewsDetail";
$(function(){
	
	 //alert($('#newsrefresh').attr('href'));
	
	$list = $("#fileList"),
	$btn = $("#btn-star"),
	state = "pending",
	uploader;
	$.ajaxSetup({   
        async : false  
    }); 
	$.post(basePath+"/sys/findDictionByTablenameAndTableField",{"tableName":"sta_new_details","tableField":"dettype"},function(data){
		 for(var i = 0; i < data.data.length; i++){
			 $("#dettype").append("<option value='"+data.data[i].fieldvalue+"'>"+data.data[i].displayvalue+"</option>");
		 }
	});
	
	
	
	var id = getQueryString('id');
	
	if(id != null ){
		$.post(basePath+"/news/findNewsById",{"id":id},function(data){
			 $('#dettitle').val(data.data.dettitle);
			 $('#topsource').val(data.data.topsource);
			 $('#dettype').val(data.data.dettype);
			 $('#displaystatus').val(data.data.displaystatus);
			 
			 var displaystatusHtml = ""; 
			 if(data.data.displaystatus == "0"){
				 displaystatusHtml = '<div class="radio-box">'+
													'<input name="displaystatus"  value = "0"  type="radio" id="sex-1" checked>'+
													'<label for="sex-1">启用</label>'+
												'</div>'+
													'<div class="radio-box">'+
													'<input type="radio"  value="1" id="sex-2" name="displaystatus">'+
													'<label for="sex-2">停用</label>'+
												'</div>';
			 }else{
				 displaystatusHtml = '<div class="radio-box">'+
													'<input name="displaystatus"  value = "0"  type="radio" id="sex-1" >'+
													'<label for="sex-1">启用</label>'+
												'</div>'+
													'<div class="radio-box">'+
													'<input type="radio"  value="1" id="sex-2" name="displaystatus" checked>'+
													'<label for="sex-2">停用</label>'+
												'</div>';
			 }
			 $('#displayStatus').html(displaystatusHtml);
			 
			 $('#displayorder').val(data.data.displayorder);
			 $('#createtime').val(data.data.createtime);
			 $('#listPicture').attr("src",basePath+'/'+data.data.detpicture);
			 $('#listPicture').val(data.data.detpicture);
			 $('#editor').val(data.data.detcontent);
			 url = basePath+"/news/editNewsDetail?id="+id+"";
		});
	}
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	
	//验证
	$('#subBtn').on('click',function(){
		//alert($('#dettype').val());
		if($('#dettype').val() == ""){
			layer.msg("请选择资讯类型",{time:1000});
			return false;
		}
		var content = ue.getContentTxt().trim()
		
		if(content == ""){
			layer.msg("资讯内容不能为空",{time:1000});
			return false;
		}
	});
	
	fromValidate($('.form'));
	
	
	
	initFileUpload();

	var uploader = WebUploader.create({
		auto: true,
		swf: 'lib/webuploader/0.1.5/Uploader.swf',
	
		// 文件接收服务端。
		server: 'fileupload.php',
	
		// 选择文件的按钮。可选。
		// 内部根据当前运行是创建，可能是input元素，也可能是flash.
		pick: '#filePicker',
	
		// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
		resize: false,
		// 只允许选择图片文件。
		accept: {
			title: 'Images',
			extensions: 'gif,jpg,jpeg,bmp,png',
			mimeTypes: 'image/*'
		}
	});
	uploader.on( 'fileQueued', function( file ) {
		var $li = $(
			'<div id="' + file.id + '" class="item">' +
				'<div class="pic-box"><img></div>'+
				'<div class="info">' + file.name + '</div>' +
				'<p class="state">等待上传...</p>'+
			'</div>'
		),
		$img = $li.find('img');
		$list.append( $li );
	
		// 创建缩略图
		// 如果为非图片文件，可以不用调用此方法。
		// thumbnailWidth x thumbnailHeight 为 100 x 100
		uploader.makeThumb( file, function( error, src ) {
			if ( error ) {
				$img.replaceWith('<span>不能预览</span>');
				return;
			}
	
			$img.attr( 'src', src );
		}, thumbnailWidth, thumbnailHeight );
	});
	// 文件上传过程中创建进度条实时显示。
	uploader.on( 'uploadProgress', function( file, percentage ) {
		var $li = $( '#'+file.id ),
			$percent = $li.find('.progress-box .sr-only');
	
		// 避免重复创建
		if ( !$percent.length ) {
			$percent = $('<div class="progress-box"><span class="progress-bar radius"><span class="sr-only" style="width:0%"></span></span></div>').appendTo( $li ).find('.sr-only');
		}
		$li.find(".state").text("上传中");
		$percent.css( 'width', percentage * 100 + '%' );
	});
	
	// 文件上传成功，给item添加成功class, 用样式标记上传成功。
	uploader.on( 'uploadSuccess', function( file ) {
		$( '#'+file.id ).addClass('upload-state-success').find(".state").text("已上传");
	});
	
	// 文件上传失败，显示上传出错。
	uploader.on( 'uploadError', function( file ) {
		$( '#'+file.id ).addClass('upload-state-error').find(".state").text("上传出错");
	});
	
	// 完成上传完了，成功或者失败，先删除进度条。
	uploader.on( 'uploadComplete', function( file ) {
		$( '#'+file.id ).find('.progress-box').fadeOut();
	});
	uploader.on('all', function (type) {
        if (type === 'startUpload') {
            state = 'uploading';
        } else if (type === 'stopUpload') {
            state = 'paused';
        } else if (type === 'uploadFinished') {
            state = 'done';
        }

        if (state === 'uploading') {
            $btn.text('暂停上传');
        } else {
            $btn.text('开始上传');
        }
    });

    $btn.on('click', function () {
        if (state === 'uploading') {
            uploader.stop();
        } else {
            uploader.upload();
        }
    });
	
	var ue = UE.getEditor('editor');
	
});



//编辑时，初始化上传图片的插件，显示移除、修改按钮，隐藏选择图片按钮
var initFileUpload = function(){
	//获取图片的地址
	var picUrl = $("#listPicture").attr("src");
	
	//如果存在图片，则进行移除、修改的显示，选择图片的隐藏
	if("" != picUrl){
		//预览图片的显示
		$(".fileupload-preview").show();
		//移除、修改的显示，选择图片的隐藏
		$(".fileupload").addClass('fileupload-exists').removeClass('fileupload-new');
	}
	
	
}


var fromValidate = function(form){
	if($('#dettype').val() == " "){
		alert("请选择资讯类型");
		return;
	}
	
	
	$(form).validate({
		rules:{
			dettitle:{
				required:true
			},
			topsource:{
				required:true
			},
			detcontent:{
				required:true
			}
			
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 