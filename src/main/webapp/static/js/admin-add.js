$(function(){
	$.ajaxSetup({   
        async : false  
    }); 
	//alert(getQueryString('id'));
	//alert(basePath);
	$.post(basePath+"/sys/findDictionByTablenameAndTableField",{"tableName":"administrator","tableField":"type"},function(data){
		 for(var i = 0; i < data.data.length; i++){
			 $("#type").append("<option value='"+data.data[i].fieldvalue+"'>"+data.data[i].displayvalue+"</option>");
		 }
	});
	
	var id = getQueryString('id');
	var url = basePath+"/admin/addAdmin";
	
	if(id != null ){
		$.post(basePath+"/admin/getAdminById",{"id":id},function(data){
			 $('#account').val(data.data.account);
			 $('#name').val(data.data.name);
			 $('#password').val(data.data.password);
			 $('#confirmp').val(data.data.password);
			 $('#gender').val(data.data.gender);
			 var sexHtml="";
			 if(data.data.gender == "0"){
				 //$('.iradio-blue :first').addClass("checked");
				 //$('.iradio-blue :last').removeClass("checked");
				// $('#man').prop("checked",true);
				 sexHtml='<div class="radio-box">'+
											'<input name="gender"  value = "0"  type="radio" id="man"  checked>'+
										   ' <label for="sex-1">男</label>'+
										'</div>'+
										'<div class="radio-box">'+
											'<input type="radio"  value="1" id="woman" name="gender">'+
											'<label for="sex-2">女</label>'+
										'</div>';
				 
			 }else{
				 sexHtml='<div class="radio-box">'+
								'<input name="gender"  value = "0"  type="radio" id="man"  >'+
							   ' <label for="sex-1">男</label>'+
							'</div>'+
							'<div class="radio-box">'+
								'<input type="radio"  value="1" id="woman" name="gender" checked>'+
								'<label for="sex-2">女</label>'+
							'</div>';
				// $('#woman').prop("checked",true);
			 }
			 
			 $('#sex').html(sexHtml);
			 
			// alert(data.data.headportrait);
			 if(data.data.headportrait != null){
				 $('#listPicture').attr("src",basePath+'/'+data.data.headportrait);
			 }
			 
			// $('#listPicture').val(data.data.headportrait);
			 
			 $('#phone').val(data.data.phone);
			 $('#email').val(data.data.email);
			 $('#type').val(data.data.type);
			 $('#description').val(data.data.description);
			 url = basePath+"/admin/editAdmin?id="+id+"";
		});
	}
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	initFileUpload();
	$("#form-admin-add").validate({
		rules:{
			account:{
				required:true,
				maxlength:16
			},
			name:{
				required:true,
				maxlength:16
			},
			password:{
				required:true,
			},
			confirmp:{
				required:true,
				equalTo: "#password"
			},
			gender:{
				required:true,
			},
			phone:{
				required:true,
				isPhone:true,
			},
			email:{
				required:true,
				email:true,
			},
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
});

//编辑时，初始化上传图片的插件，显示移除、修改按钮，隐藏选择图片按钮
var initFileUpload = function(){
	//获取图片的地址
	var picUrl = $("#listPicture").attr("src");
	
	//如果存在图片，则进行移除、修改的显示，选择图片的隐藏
	if("" != picUrl){
		//预览图片的显示
		$(".fileupload-preview").show();
		//移除、修改的显示，选择图片的隐藏
		$(".fileupload").addClass('fileupload-exists').removeClass('fileupload-new');
	}
	
	
}

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 
