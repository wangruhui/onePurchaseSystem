var curPage = 1;
var orderNo ="",orderStatus="",payDate="",creOrdreDate="";
$(function(){
	   
	   //加载表格数据
	    getOrderList(curPage);
	  //分页操作
		$("body").on("click",".paginate_button",function(){
			getOrderList($(this).data('page'),orderNo,orderStatus,payDate,creOrdreDate);
		});
		
		//搜索
		$("body").on("click","#search",function(){
			orderNo = $('#orderNo').val();
			orderStatus = $('#orderStatus').val();
			creOrdreDate = $('#creOrdreDate').val();
			payDate = $('#payDate').val();
			curPage =1;
			getOrderList(curPage,orderNo,orderStatus,payDate,creOrdreDate);
		})
	   
});


//分页加载
var getOrderList = function(curPage,orderNo,orderStatus,payDate,creOrdreDate){
	$.post(basePath+"/order/getCommOrderList",{"curPage":curPage,"orderNo":orderNo,"orderStatus":orderStatus,"payDate":payDate,"creOrdreDate":creOrdreDate},function(data){
    	$('tbody').html("");
    	var html="";
    	
    	
    	
		 for(var i=0;i<data.data.length;i++){
			 var expressBtn="";
			 
			 if(data.data[i].ordstate =="待发货"){
				 expressBtn = '<a title="发货" href="javascript:;" onclick="express(\'发货\',\''+basePath+'/expressEdit?id='+data.data[i].id+'\',\'0001\',\'500\',\'300\')"  class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe669;</i></a>';
			 }
			 if(data.data[i].ordstate =="退款中"){
				 expressBtn = '<a title="完成退款" href="javascript:;" onclick="refund_finish(\'退款\',\''+basePath+'/setRefund?id='+data.data[i].id+'\',\'800\',\'800\')"  class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe66b;</i></a>';
			 }
			 
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox"  data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].orderNumber+'</td>'+
			        '<td><span class="label label-success radius">'+data.data[i].ordstate+'</span></td>'+
			        '<td>'+data.data[i].nickName+'</td>'+
			        '<td>'+data.data[i].commoditynumber+'</td>'+
			        '<td>'+data.data[i].createtime+'</td>'+
			        '<td>'+data.data[i].totalprice+'</td>'+
			        '<td>'+data.data[i].freight+'</td>'+
			        '<td>'+data.data[i].orderTotalPrice+'</td>'+
			        '<td>'+data.data[i].payamount+'</td>'+
			        '<td>'+data.data[i].mortgageamount+'</td>'+
			        '<td>'+data.data[i].paytime+'</td>'+
			        '<td>'+data.data[i].paytype+'</td>'+
			        '<td class="f-14"><a style="text-decoration:none" onclick="show_orderDetail(\'订单详情\',\''+basePath+'/order/getOrderDetail?id='+data.data[i].id+'\',\'10001\',\'500\',\'500\')" href="javascript:;" title="订单详情"><i class="Hui-iconfont">&#xe687;</i></a>'+expressBtn+'</td>'+
			     ' </tr>';
			  
		  }
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="14" class="dataTables_empty">没有数据</td></tr>';
			}
		 
		 $('tbody').html(html);
		 $('#DataTables_Table_0_paginate').html(data.pages);
	   });
}
function refund_finish(title,url,w,h){
	layer_show(title,url,w,h);
}

function express(title,url,id,w,h){
	  layer_show(title,url,w,h);
	}

function show_orderDetail(title,url,id,w,h){
	layer_show(title,url,w,h);
}