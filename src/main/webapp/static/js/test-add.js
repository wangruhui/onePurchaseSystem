
var url = basePath+"/test/addNewsDetail";
$(function(){
	 //alert($('#newsrefresh').attr('href'));
	
	$.ajaxSetup({   
        async : false  
    }); 
	
	var id = getQueryString('id');
	
	if(id != null ){
		$.post(basePath+"/test/findNewsById",{"id":id},function(data){
			 $('#dettitle').val(data.data.dettitle);
			 $('#displayorder').val(data.data.displayorder);
			 $('#createtime').val(data.data.createtime);
			 $('#editor').val(data.data.detcontent);
			 url = basePath+"/test/editNewsDetail?id="+id+"";
		});
	}
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	
	//验证
	$('#subBtn').on('click',function(){
		var content = ue.getContentTxt().trim()
		
		if(content == ""){
			layer.msg("资讯内容不能为空",{time:1000});
			return false;
		}
	});
	
	fromValidate($('.form'));
	var ue = UE.getEditor('editor');
	
});


var fromValidate = function(form){
	
	$(form).validate({
		rules:{
			dettitle:{
				required:true
			},
			detcontent:{
				required:true
			}
			
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 