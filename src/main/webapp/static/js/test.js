var curPage = 1;
var title = "";
var publishDate = "";
$(function(){
	//alert($('#newsrefresh').attr('href'));
	getNewsDetailList(curPage,title,publishDate);
	
	$("body").on("click","#search",function(){
		title = $('#title').val();
		publishDate = $('#publishDate').val();
		curPage =1;
		getNewsDetailList(curPage,title,publishDate);
	})
	
	
	//分页操作
	$("body").on("click",".paginate_button",function(){
		getNewsDetailList($(this).data('page'),title,publishDate);
	});
});

var getNewsDetailList = function(curPage,title,publishDate){
	var html="";
	$.post(basePath+"/test/getNewsDetailList",{"curPage":curPage,"title":title,"publishDate":publishDate},function(data){
		for(var i = 0; i < data.data.length; i++){
			html+='<tr class="text-c">'+
			'<td><input type="checkbox" value="1" data-id=\''+data.data[i].id+'\'></td>'+
			'<td>'+data.data[i].dettitle+'</td>'+
			'<td>'+data.data[i].createtime+'</td>'+
			'<td>'+data.data[i].createname+'</td>'+
			'<td>'+data.data[i].updatename+'</td>'+
			'<td>'+data.data[i].displayorder+'</td>'+
			'<td class="td-manage"><a title="编辑" href="javascript:;" onclick="news_edit(\'资讯编辑\',\''+basePath+'/testdetailEdit?id='+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="news_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a><a title="预览" href="javascript:;" onclick="show_newDetails(\'资讯预览\',\''+basePath+'/test/showNews?id='+data.data[i].id+'\',\'800\',\'500\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe695;</i></a></td>'+
		'</tr>';
		}
		
		if(html == ""){
			html = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">没有数据</td></tr>';
		}
		
		$('tbody').html(html);
		$('#DataTables_Table_0_paginate').html(data.pages);
		
	});
	
	
}
function show_newDetails(title,url,w,h){
	layer_show(title,url,w,h);
}
/*资讯-增加*/
function newsDetail_add(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*资讯-删除*/
function news_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		
		$.ajax({
			type:"post",
			url:basePath+"/test/delNewsDetail",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}
/*管理员-编辑*/
function news_edit(title,url,id,w,h){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/test/delNewsDetail",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getNewsDetailList(curPage,title,publishDate);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}