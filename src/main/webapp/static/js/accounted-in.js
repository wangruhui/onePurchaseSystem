var curPage = 1;
$(function(){
	   
	   //加载表格数据
	   getAccountInList(curPage);
	  //分页操作
		$("body").on("click",".paginate_button",function(){
			getAccountInList($(this).data('page'));
		});
		
	   
});


//分页加载
var getAccountInList = function(curPage){
	$.post(basePath+"/account/getAccountInList",{"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
    	
		 for(var i=0;i<data.data.length;i++){
			
			 
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox"  data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].nickName+'</td>'+
			        '<td>'+data.data[i].ordnumber+'</td>'+
			        '<td>'+data.data[i].payorderid+'</td>'+
			        '<td>'+data.data[i].inamount+'</td>'+
			        '<td>'+data.data[i].intype+'</td>'+
			        '<td>'+data.data[i].intime+'</td>'+
			     ' </tr>';
			  
		  }
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
			}
		 
		 $('tbody').html(html);
		 $('#DataTables_Table_0_paginate').html(data.pages);
	   });
}

