var curPage = 1;
$(function(){
	   //加载表
	   $.post(basePath+"/sys/getTableName",function(data){
			 for(var i=0;i<data.length;i++){
				  $('#tableName').append("<option value='"+data[i].tableName+"'>"+data[i].tableName+"</option>");
			  }
		   });
	   
	   //加载表字段
	   $('#tableName').change(function(){
		   var tableName=$(this).children('option:selected').val();
		   if(tableName == "0"){
			   return false;
		   }else{
			   //加载表字段
			   $.post(basePath+"/sys/getFieldName",{"tableName":tableName},function(data){
					 for(var i=0;i<data.length;i++){
						  $('#fieldName').append("<option value='"+data[i].fieldName+"'>"+data[i].fieldName+"</option>");
					  }
				   });
			   
		   }
	   });
	   
	   
	   //加载表格数据
	    getDictionList(curPage);
	  //分页操作
		$("body").on("click",".paginate_button",function(){
			getDictionList($(this).data('page'));
		});
	   
	   $('body').on('click','#addDiction',addDiction);
});


//分页加载
var getDictionList = function(curPage){
	$.post(basePath+"/sys/getDictionList",{"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
		 for(var i=0;i<data.data.length;i++){
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox"  data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].dictionaryname+'</td>'+
			        '<td>'+data.data[i].tablename+'</td>'+
			        '<td>'+data.data[i].fieldname+'</td>'+
			        '<td>'+data.data[i].createdate+'</td>'+
			        '<td class="f-14"><a style="text-decoration:none" onclick="system_data_edit(\'字典编辑\',\''+basePath+'/systemDataList?dictionaryId='+data.data[i].id+'\',\'0001\',\'800\',\'800\')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="system_data_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>'+
			     ' </tr>';
			  
		  }
		 $('tbody').html(html);
		 $('#DataTables_Table_0_paginate').html(data.pages);
	   });
}
//添加字典
var addDiction = function(){
	var dictionaryName = $('#dictionaryName').val();
	var tableName = $('#tableName').children('option:selected').val();
	var fieldName = $('#fieldName').children('option:selected').val();
	
	if(dictionaryName == "" || tableName == "" || fieldName == "" ){
		layer.confirm('请填写相关信息!');
		return false;
	}{
		var params={
				"dictionaryName":dictionaryName,
		        "tableName":tableName,
		        "fieldName":fieldName,
		};
		
		$.post(basePath+"/sys/addDiction",params,function(data){
			if(data.code == "-3"){
       		 layer.msg("管理员登录超时，请重新登录",{time:1000});
       		 setTimeout(function(){
       			 window.parent.parent.location.href=basePath+"/login";
	        	 },2000)
	        	 return false;
       	}
			 if(data.code=="0"){
				 layer.msg('添加成功', {
					    time: 2000, //2s后自动关闭
					    btn: [ '确定']
					  });
				 getDictionList(curPage);
			 }else{
				 layer.msg(data.msg, {
					    time: 2000, //2s后自动关闭
					    btn: [ '确定']
					  });
			 }
		   });
	}
	
}


/*数据字典-编辑*/
function system_data_edit(title,url,id,w,h){
  layer_show(title,url,w,h);
}
/*数据字典-删除*/
function system_data_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/sys/delDictionary",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/sys/delDictionary",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getDictionList(curPage);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}