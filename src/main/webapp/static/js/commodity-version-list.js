var dictionaryId = "";
var curPage = 1;
$(function(){
	
	commodityId = getQueryString('commodityId');
	
	//alert(dictionaryId);
	
	   //加载表格数据
	getCommodityVersionList(curPage);
	    //分页操作
		$("body").on("click",".paginate_button",function(){
			getCommodityVersionList($(this).data('page'));
		});
	   //$('body').on('click','#addDiction',addDiction);
	   
});

var getCommodityVersionList = function(curPage){
	$.post(basePath+"/commodity/getVersionList",{"commodityId":commodityId,"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
		 for(var i=0;i<data.data.length;i++){
			  html+='<tr class="text-c"  id="tablevalue">'+
			        '<td><input type="checkbox" data-id=\''+data.data[i].id+'\'></td>'+
			        '<td>'+data.data[i].versionType+'</td>'+
			        '<td>'+data.data[i].versionPrice+'</td>'+
			        '<td>'+data.data[i].versionStock+'</td>'+
			        '<td>'+data.data[i].versionSort+'</td>'+
			        '<td class="f-14"><a style="text-decoration:none" onclick="commodity_version_edit(\'编辑型号\',\'/commodityVersionAdd?commodityId='+commodityId+'&versionId='+data.data[i].id+'\',\'0001\',\'600\',\'600\')" href="javascript:;" title="编辑"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="version_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>'+
			        '<a title="图片上传" href="javascript:;" onclick="upload_img(\'图片上传\',\''+basePath+'/uploadPic?id='+data.data[i].id+'\',\'0001\',\'600\',\'600\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe634;</i></a><a title="图片查看" href="javascript:;" onclick="upload_img(\'图片查看\',\'getVersionPicList?id='+data.data[i].id+'\',\'0001\',\'800\',\'800\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe613;</i></a>'+
			     ' </tr>';
			  
		  }
		 
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
			}
		 
		 html+=data.pages;
		 $('tbody').html(html);
	   });
}

/*型号-添加*/
function commodity_version_edit(title,url,id,w,h){
  layer_show(title,url,w,h);
}

function commodity_version_add(title,url,id,w,h){
	  layer_show(title,url+"?commodityId="+commodityId,w,h);
	}

function upload_img(title,url,id,w,h){
	  layer_show(title,url,w,h);
	  }

/*型号-删除*/
function version_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/commodity/delVersion",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/commodity/delVersion",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getCommodityVersionList(curPage);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}