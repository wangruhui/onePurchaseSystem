
var url = basePath+"/topic/addTopic";
$(function(){
	
	 //alert($('#newsrefresh').attr('href'));
	
	
	var id = getQueryString('id');
	
	if(id != null ){
		$.ajaxSetup({   
	        async : false  
	    }); 
		$.post(basePath+"/topic/findTopicById",{"id":id},function(data){
			 $('#toptitle').val(data.data.toptitle);
			 
			 var displaystatusHtml = ""; 
			 if(data.data.displaystatus == "0"){
				 displaystatusHtml = '<div class="radio-box">'+
													'<input name="displaystatus"  value = "0"  type="radio" id="sex-1" checked>'+
													'<label for="sex-1">启用</label>'+
												'</div>'+
													'<div class="radio-box">'+
													'<input type="radio"  value="1" id="sex-2" name="displaystatus">'+
													'<label for="sex-2">停用</label>'+
												'</div>';
			 }else{
				 displaystatusHtml = '<div class="radio-box">'+
													'<input name="displaystatus"  value = "0"  type="radio" id="sex-1" >'+
													'<label for="sex-1">启用</label>'+
												'</div>'+
													'<div class="radio-box">'+
													'<input type="radio"  value="1" id="sex-2" name="displaystatus" checked>'+
													'<label for="sex-2">停用</label>'+
												'</div>';
			 }
			 $('#displayStatus').html(displaystatusHtml);
			 
			 
			 $('#displayorder').val(data.data.displayorder);
			 $('#createtime').val(data.data.createtime);
			 $('#listPicture').attr("src",basePath+'/'+data.data.toppicture);
			 $('#listPicture').val(data.data.toppicture);
			 $('#topcontent').val(data.data.topcontent);
			 url = basePath+"/topic/editTopic?id="+id+"";
		});
	}
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	
	//验证
	
	fromValidate($('.form'));
	initFileUpload();
	
});

//编辑时，初始化上传图片的插件，显示移除、修改按钮，隐藏选择图片按钮
var initFileUpload = function(){
	//获取图片的地址
	var picUrl = $("#listPicture").attr("src");
	
	//如果存在图片，则进行移除、修改的显示，选择图片的隐藏
	if("" != picUrl){
		//预览图片的显示
		$(".fileupload-preview").show();
		//移除、修改的显示，选择图片的隐藏
		$(".fileupload").addClass('fileupload-exists').removeClass('fileupload-new');
	}
	
	
}


var fromValidate = function(form){

	$(form).validate({
		rules:{
			toptitle:{
				required:true
			},
			topcontent:{
				required:true
			}
			
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 