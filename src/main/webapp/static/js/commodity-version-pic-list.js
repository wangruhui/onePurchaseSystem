var versionId = "";
var curPage = 1;
$(function(){
	
	versionId = getQueryString('id');
	
	//alert(dictionaryId);
	
	   //加载表格数据
	getVersionPicList(curPage,versionId);
	    //分页操作
		$("body").on("click",".paginate_button",function(){
			getVersionPicList($(this).data('page'),versionId);
		});
	   //$('body').on('click','#addDiction',addDiction);
		
		
		//编辑排序
		$("body").on("click",".sort",function(){
			if($(this).html().indexOf("input")==-1){
				var varvalue=$(this).html();
				var id = $(this).data("id");
				
				$(this).html('<input type="text" data-id="'+id+'" class="input-text" onblur="edit()" style="width:250px" value="'+varvalue+'">');
				$(".input-text").focus();
			}
		});
});

function edit(){
	var sortval = $(".input-text").val();	
	var id = $(".input-text").data('id');
	if(sortval == ""||sortval == null){
		layer.msg("请填写排序值",{time:1000});
		$(".input-text").s();
		return false;
	}else{
		$.ajax({
			type:"post",
			url:basePath+"/commodity/updatePicSort",
			data:{"id":id,"picSort":sortval},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					parent.location.reload();
					getVersionPicList(curPage,versionId);
					layer.msg('修改成功!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
	}
	
}

var getVersionPicList = function(curPage,versionId){
	$.post(basePath+"/commodity/getVersionPList",{"versionId":versionId,"curPage":curPage},function(data){
    	$('tbody').html("");
    	var html="";
		 for(var i=0;i<data.data.length;i++){
			  html+='<tr class="text-c">'+
			        '<td><input type="checkbox" data-id=\''+data.data[i].id+'\'></td>'+
			        '<td><img style="width:50px;height:50px;" src="'+basePath+'/'+data.data[i].picUrl+'"  alt="" /></td>'+
			       '<td class = "sort" data-id="'+data.data[i].id+'">'+data.data[i].picSort+'</td>'+
			         //'<td class = "sort" data-sort="'+data.data[i].picSort+'"><input type="text" ></td>'+
			        '<td>'+data.data[i].updateName+'</td>'+
			        '<td>'+data.data[i].updateDate+'</td>'+
			        '<td class="f-14"><a title="删除" href="javascript:;" onclick="version_pic_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>'+
			     ' </tr>';
			  
		  }
		 
		 if(html == ""){
				html = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">没有数据</td></tr>';
			}
		 
		 html+=data.pages;
		 $('tbody').html(html);
	   });
}



/*型号-删除*/
function version_pic_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/commodity/delVersionPic",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}

function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
}

function upload_img(title,url,id,w,h){
	  layer_show(title,url+'?id='+versionId,w,h);
  }

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/commodity/delVersionPic",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getVersionPicList(curPage,versionId);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}