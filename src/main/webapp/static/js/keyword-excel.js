
var url = basePath+"/basic/uploadExcel";
$(function(){
	fromValidate($('.form'));
	
	$(".progress").css("display","none");
});

///上传文件格式验证  
$.validator.addMethod("filetype",function(value,element, param){
	var fileType = value.substring(value.lastIndexOf(".") + 1).toLowerCase();  
    return this.optional(element) || $.inArray(fileType, param) != -1; 
},"该值已存在");

var fromValidate = function(form){

	
	
	$(form).validate({
		rules:{
			file:{
				required: true,  
                filetype: ["xlsx"] 
			}
		},
		messages:{
			file:{
				required:"请选择文件",
				filetype:"请上传Excel文件"
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		beforesend:$(".progress").css("display","block"),
		submitHandler:function(form){
			$(form).ajaxSubmit({
		        type:"post",
		        url:url,
		        //beforeSubmit: showRequest,
		        beforesend:$(".progress").css("display","block"),
		        success: function(data){
		        	if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
		        	 $(".progress").css("display","none");
		        	 layer.msg(data.msg,{time:1000});
		        	 setTimeout(function(){
		        		 parent.location.reload();
		        		 var index = parent.layer.getFrameIndex(window.name);
			        	 parent.layer.close(index);
			        	 
		        	 },2000)
		        	 
		        	
		        }
		      });
		}
	});
}


function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) return unescape(r[2]); return null; 
	} 