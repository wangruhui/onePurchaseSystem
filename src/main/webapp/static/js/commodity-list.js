var curPage = 1;
var title = "";
var publishDate = "";
$(function(){
	//alert($('#newsrefresh').attr('href'));
	getCommodityList(curPage,title,publishDate);
	
	$("body").on("click","#search",function(){
		title = $('#title').val();
		publishDate = $('#publishDate').val();
		curPage =1;
		getCommodityList(curPage,title,publishDate);
	})
	
	
	//分页操作
	$("body").on("click",".paginate_button",function(){
		getCommodityList($(this).data('page'),title,publishDate);
	});
});

var getCommodityList = function(curPage,title,publishDate){
	var html="";
	$.post(basePath+"/commodity/getCommodityList",{"curPage":curPage,"title":title,"publishDate":publishDate},function(data){
		for(var i = 0; i < data.data.length; i++){
			var isenable='<span class="label radius">下架</span>';
			var bBtn = '<a style="text-decoration:none" onclick="commodity_start(this,\''+data.data[i].id+'\')" href="javascript:;" title="上架"><i class="Hui-iconfont">&#xe615;</i></a>';
			if(data.data[i].commodityStatus == "0"){
				isenable = '<span class="label label-success radius">上架</span>';
				bBtn = '<a style="text-decoration:none" onclick="commodity_stop(this,\''+data.data[i].id+'\')" href="javascript:;" title="下架"><i class="Hui-iconfont">&#xe631;</i></a>';
			}
			
			html+='<tr class="text-c">'+
			'<td><input type="checkbox" value="1" data-id=\''+data.data[i].id+'\'></td>'+
			'<td>'+data.data[i].commodityName+'</td>'+
			'<td>'+data.data[i].commodityTitle+'</td>'+
			'<td><img style="width:50px;height:50px;" src="'+basePath+'/'+data.data[i].commodityPicture+'"  alt="'+data.data[i].commodityName+'" /></td>'+
			'<td>'+data.data[i].createDate+'</td>'+
			'<td>'+data.data[i].salesVolume+'</td>'+
			'<td>'+data.data[i].createName+'</td>'+
			'<td>'+data.data[i].updateName+'</td>'+
			'<td>'+data.data[i].sort+'</td>'+
			'<td class="td-status">'+isenable+'</td>'+
			'<td class="td-manage">'+bBtn+'<a title="编辑" href="javascript:;" onclick="commodity_edit(\'商品编辑\',\'commodityAdd?id='+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="commodity_del(this,\''+data.data[i].id+'\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>'+
			'<a title="查看商品型号" href="javascript:;" onclick="show_version(\'查看型号\',\''+basePath+'/commodityVersionList?commodityId='+data.data[i].id+'\',\'0001\',\'800\',\'800\')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe665;</i></a></td>'+
		'</tr>';
		}
		
		if(html == ""){
			html = '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">没有数据</td></tr>';
		}
		
		$('tbody').html(html);
		$('#DataTables_Table_0_paginate').html(data.pages);
		
	});
	
	
}

/*查看商品评论*/
function show_version(title,url,id,w,h){
  layer_show(title,url,w,h);
}

/*商品-增加*/
function commodity_add(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*商品-删除*/
function commodity_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		
		$.ajax({
			type:"post",
			url:basePath+"/commodity/delCommodity",
			data:{"ids":id},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").remove();
					layer.msg('已删除!',{icon:1,time:1000});
				}else{
					alert('error');
				}
			}
		});
		
		
	});
}
/*商品-编辑*/
function commodity_edit(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*商品-停用*/
function commodity_stop(obj,id){
	layer.confirm('确认要下架吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/commodity/handleCommodityStatus",
			data:{"id":id,"status":"1"},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a onClick="commodity_start(this,\''+id+'\')" href="javascript:;" title="上架" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">下架</span>');
					$(obj).remove();
					layer.msg('已下架!',{icon: 5,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}

/*商品-启用*/
function commodity_start(obj,id){
	layer.confirm('确认要上架吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type:"post",
			url:basePath+"/commodity/handleCommodityStatus",
			data:{"id":id,"status":"0"},
			async:true,
			success:function(data){
				if(data.code == "-3"){
	        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
	        		 setTimeout(function(){
	        			 window.parent.parent.location.href=basePath+"/login";
		        	 },2000)
		        	 return false;
	        	}
				if(data.code=="0"){
					$(obj).parents("tr").find(".td-manage").prepend('<a onClick="commodity_stop(this,\''+id+'\')" href="javascript:;" title="下架" style="text-decoration:none"><i class="Hui-iconfont">&#xe631;</i></a>');
					$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">上架</span>');
					$(obj).remove();
					layer.msg('已上架!', {icon: 6,time:1000});
				}else{
					alert('error');
				}
			}
		});
	});
}

/**
 * 批量删除
 */
function datadel(){
	var ids = "";
	var length = 0;
	
	//获取被选中的数据Id
	$("tbody :checkbox").each(function(){
		
		if($(this).is(':checked')){
			ids+=$(this).data("id")+",";
			length++;
		}
		
	});
	
	if(length >0){
		
		layer.confirm('确认要删除选中的'+length+'条数据吗？',function(index){
			$.ajax({
				type:"post",
				url:basePath+"/commodity/delCommodity",
				data:{"ids":ids},
				async:true,
				success:function(data){
					if(data.code == "-3"){
		        		 layer.msg("管理员登录超时，请重新登录",{time:1000});
		        		 setTimeout(function(){
		        			 window.parent.parent.location.href=basePath+"/login";
			        	 },2000)
			        	 return false;
		        	}
					if(data.code=="0"){
						getCommodityList(curPage,title,publishDate);
						layer.msg('已删除!',{icon:1,time:1000});
					}else{
						alert('error');
					}
				}
			});
		});
		
	}else{
		layer.msg('请选择要删除的数据!',{icon:2,time:1000});
		return false;
	}
}