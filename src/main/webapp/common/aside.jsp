<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<input runat="server" id="divScrollValue" type="hidden" value="" />
	<div class="menu_dropdown bk_2">
	  <c:forEach items="${getPermissionList}" var ="list">
		<dl id="menu-article">
			<dt><i class="Hui-iconfont">${list.ioc }</i> ${list.permissionName }<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
				<c:forEach items="${list.permission}" var ="permission">	
					<li><a _href="<%=basePath%>${permission.visiturl}" data-title="${permission.permissionname}" href="javascript:void(0)">${permission.permissionname}</a></li>
					</c:forEach>
				</ul>
			</dd>
		</dl>
		</c:forEach>
	</div>