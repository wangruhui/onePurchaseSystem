<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="navbar navbar-fixed-top">
		<div class="container-fluid cl"> <a id="sysName" class="logo navbar-logo f-l mr-10 hidden-xs" href="#">${sc.systemname }</a><a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
			<nav class="nav navbar-nav">
			</nav>
			<nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
				<ul class="cl">
					<li>${admin.type }</li>
					<li class="dropDown dropDown_hover"> <a href="#" class="dropDown_A">${admin.name }<i class="Hui-iconfont">&#xe6d5;</i></a>
						<ul class="dropDown-menu menu radius box-shadow">
							<li><a href="<%=basePath %>admin/adminLoginOut">退出</a></li>
						</ul>
					</li>
					<li id="Hui-msg"><img style="width:35px;height:35px;" src="<c:if test='${admin.headportrait ==null}'><%=basePath %>static/h-ui/images/user.png</c:if><c:if test='${admin.headportrait !=null}'><%=basePath %>${admin.headportrait}</c:if>"     alt="${admin.name }" class="round"></li>
					<li id="Hui-skin" class="dropDown right dropDown_hover"> <a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
						<ul class="dropDown-menu menu radius box-shadow">
							<li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
							<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
							<li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
							<li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
							<li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
							<li><a href="javascript:;" data-val="orange" title="绿色">橙色</a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
</div>