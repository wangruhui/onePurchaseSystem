package com.ideazd.common.utils;

/**
 * 前台异步分页工具类<br/>
 * author:yzz<br/>
 * date:2014-4-8<br/>
 * */

public class PagingUtil {
	private Integer rowCount = 0; //总记录数
	private Integer currentPage = 1; //当前页数
	private Integer pageCount = 0; //总页面
	private Integer sizePerPage = 10;//每页显示条数
	private String pageDisplay; //JSP页面显示
	private Integer pagePreOffset = 2; //向前偏移量
	private Integer pageNextOffset = 2; //向后偏移量
	public String getPageDisplay() {
        StringBuffer pageString = new StringBuffer();
       
        // 如果只有一页，不需要分页  
        if(getPageCount() > 1) { 
        	//pageString.append("<div class='dataTables_paginate paging_simple_numbers' id='DataTables_Table_0_paginate'>");
            if(currentPage > 1) {
            	//如果当前页数大于1  
                pageString.append("<a class='paginate_button previous disabled' aria-controls='DataTables_Table_0' data-page='"+(currentPage-1)+"' id='DataTables_Table_0_previous'>上一页</a>");  
            
            }else {  
            	//pageString.append("<a class='paginate_button previous disabled' aria-controls='DataTables_Table_0' data-page='"+currentPage+"' id='DataTables_Table_0_previous'>上一页</a>"); 
            }  
            
            // 定义向前偏移量  
            int preOffset = currentPage -1 > pagePreOffset ? pagePreOffset:currentPage -1;
            
            // 定义向后偏移量  
            int nextOffset = getPageCount() - currentPage > pageNextOffset ?pageNextOffset:getPageCount() - currentPage;  
            
            //起始分页数
            int start = currentPage - preOffset;
            
            //结束分页数
            int end = currentPage + nextOffset;
            
            //处理起始偏移出现的显示个数
            if(currentPage <= 2 && pageCount >= 5) {
            	start = 1;
            	end = 5;
            }
            
            //处理结束偏移出现的显示个数
            if(currentPage > (pageCount - 2) && pageCount >= 5) {
            	start = pageCount - 4;
            	end = pageCount;
            }
            
            // 循环显示链接数字，范围是从 当前页减向前偏移量 到 当前页加向后偏移量  
            for(int i = start; i <= end; i++) {  
                
                if(currentPage == i) {//显示为当前活动的页数
                    pageString  .append("<span><a class='paginate_button current' aria-controls='DataTables_Table_0' data-page="+i+">"+i+"</a></span>");  
                }else {  
                	 pageString  .append("<span><a class='paginate_button' aria-controls='DataTables_Table_0' data-page="+i+">"+i+"</a></span>");  
                }  
                
            }  
            
            // 如果当前页小于总页数，>>可用  
            if(currentPage < getPageCount()) {  
                pageString.append("<a class='paginate_button next disabled' aria-controls='DataTables_Table_0' data-page='"+(currentPage+1)+"' id='DataTables_Table_0_next'>下一页</a>");
                
            }else {  
                //pageString.append("<a class='paginate_button next disabled' aria-controls='DataTables_Table_0' data-page='"+currentPage+"' id='DataTables_Table_0_next'>下一页</a>");  
            }  
        }  
        //pageString.append("</div>");
        this.pageDisplay = pageString.toString();  
		
		return pageDisplay;
	}
	
	public Integer getPageCount() {
		//总页面 = 总条数 % 每页条数 
		//如果刚刚好的话,总页数 = 总条数/每页条数
		//反之没有刚刚好的话,总页数 = 总条数/每页条数 + 1
		pageCount = rowCount % sizePerPage == 0 ? (rowCount /sizePerPage)  
                : (rowCount /sizePerPage) +1; 
		
		return pageCount;
	}
	
	public Integer getRowCount() {
		return rowCount;
	}
	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}
	
	public Integer getCurrentPage() {
		return currentPage;
	}
	
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}
	
	public Integer getSizePerPage() {
		return sizePerPage;
	}
	
	public void setSizePerPage(Integer sizePerPage) {
		this.sizePerPage = sizePerPage;
	}
	
	public void setPageDisplay(String pageDisplay) {
		this.pageDisplay = pageDisplay;
	}
	
	public Integer getPagePreOffset() {
		return pagePreOffset;
	}
	
	public void setPagePreOffset(Integer pagePreOffset) {
		this.pagePreOffset = pagePreOffset;
	}
	
	public Integer getPageNextOffset() {
		return pageNextOffset;
	}
	
	public void setPageNextOffset(Integer pageNextOffset) {
		this.pageNextOffset = pageNextOffset;
	}
	
	
	
	
}