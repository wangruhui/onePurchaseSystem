package com.ideazd.common.cache;

import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.BinaryConnectionFactory;
import net.spy.memcached.CASMutation;
import net.spy.memcached.CASMutator;
import net.spy.memcached.CASValue;
import net.spy.memcached.ConnectionObserver;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.transcoders.LongTranscoder;
import net.spy.memcached.transcoders.SerializingTranscoder;
import net.spy.memcached.transcoders.Transcoder;

public class SpyMemcached {
	private static final Logger log = LoggerFactory.getLogger(SpyMemcached.class);
	private static MemcachedClient memClient;
	private static long defaultTimeout;
	private static TimeUnit defaultTimeUnit;
	static {
		Prop prop = PropKit.use("spy-memcached.properties", "UTF-8");
		if(prop.containsKey("memcached-servers")) {
			log.info(prop.get("memcached-servers"));
			defaultTimeout = prop.getLong("memcached-defaultTimeout");
			String timeUnit = prop.get("memcached-defaultTimeUnit");
			if (timeUnit.equals("SECONDS")) {
				defaultTimeUnit = TimeUnit.SECONDS;
			} else if (timeUnit.equals("MINUTES")) {
				defaultTimeUnit = TimeUnit.MINUTES;
			}
			try {
				if (memClient == null) {
					memClient = new MemcachedClient(new BinaryConnectionFactory(), AddrUtil.getAddresses(prop.get("memcached-servers")));
				}
			} catch (IOException e) {
				log.error("缓存服务启动失败");
				e.printStackTrace();
			}
		} else {
			log.info("not use memcached or memcached-server");
		}
	}

	private SpyMemcached() {
	}

	public static void disConnect() {
		if (memClient == null) {
			return;
		}
		memClient.shutdown();
	}

	public static void addObserver(ConnectionObserver obs) {
		memClient.addObserver(obs);
	}

	public static void removeObserver(ConnectionObserver obs) {
		memClient.removeObserver(obs);
	}

	// ---- Basic Operation Start ----//
	public static boolean set(String key, Object value, int expire) {
		Future<Boolean> f = memClient.set(key, expire, value);
		return getBooleanValue(f);
	}

	public static Object get(String key) {
		return memClient.get(key);
	}

	public static Object asyncGet(String key) {
		Object obj = null;
		Future<Object> f = memClient.asyncGet(key);
		try {
			obj = f.get(defaultTimeout, defaultTimeUnit);
		} catch (Exception e) {
			f.cancel(false);
		}
		return obj;
	}

	public static boolean add(String key, Object value, int expire) {
		Future<Boolean> f = memClient.add(key, expire, value);
		return getBooleanValue(f);
	}

	public static boolean replace(String key, Object value, int expire) {
		Future<Boolean> f = memClient.replace(key, expire, value);
		return getBooleanValue(f);
	}

	public static boolean delete(String key) {
		Future<Boolean> f = memClient.delete(key);
		return getBooleanValue(f);
	}

	public static boolean flush() {
		Future<Boolean> f = memClient.flush();
		return getBooleanValue(f);
	}

	public static Map<String, Object> getMulti(Collection<String> keys) {
		return memClient.getBulk(keys);
	}

	public static Map<String, Object> getMulti(String[] keys) {
		return memClient.getBulk(keys);
	}

	public static Map<String, Object> asyncGetMulti(Collection<String> keys) {
		Map map = null;
		Future<Map<String, Object>> f = memClient.asyncGetBulk(keys);
		try {
			map = f.get(defaultTimeout, defaultTimeUnit);
		} catch (Exception e) {
			f.cancel(false);
		}
		return map;
	}

	public static Map<String, Object> asyncGetMulti(String keys[]) {
		Map map = null;
		Future<Map<String, Object>> f = memClient.asyncGetBulk(keys);
		try {
			map = f.get(defaultTimeout, defaultTimeUnit);
		} catch (Exception e) {
			f.cancel(false);
		}
		return map;
	}

	// ---- Basic Operation End ----//

	// ---- increment & decrement Start ----//
	public static long increment(String key, int by, long defaultValue, int expire) {
		return memClient.incr(key, by, defaultValue, expire);
	}

	public static long increment(String key, int by) {
		return memClient.incr(key, by);
	}

	public static long decrement(String key, int by, long defaultValue, int expire) {
		return memClient.decr(key, by, defaultValue, expire);
	}

	public static long decrement(String key, int by) {
		return memClient.decr(key, by);
	}

	public static long asyncIncrement(String key, int by) {
		Future<Long> f = memClient.asyncIncr(key, by);
		return getLongValue(f);
	}

	public static long asyncDecrement(String key, int by) {
		Future<Long> f = memClient.asyncDecr(key, by);
		return getLongValue(f);
	}

	// ---- increment & decrement End ----//

	public static void printStats() throws IOException {
		printStats(null);
	}

	public static void printStats(OutputStream stream) throws IOException {
		Map<SocketAddress, Map<String, String>> statMap = memClient.getStats();
		if (stream == null) {
			stream = System.out;
		}
		StringBuffer buf = new StringBuffer();
		Set<SocketAddress> addrSet = statMap.keySet();
		Iterator<SocketAddress> iter = addrSet.iterator();
		while (iter.hasNext()) {
			SocketAddress addr = iter.next();
			buf.append(addr.toString() + "/n");
			Map<String, String> stat = statMap.get(addr);
			Set<String> keys = stat.keySet();
			Iterator<String> keyIter = keys.iterator();
			while (keyIter.hasNext()) {
				String key = keyIter.next();
				String value = stat.get(key);
				buf.append("  key=" + key + ";value=" + value + "/n");
			}
			buf.append("/n");
		}
		stream.write(buf.toString().getBytes());
		stream.flush();
	}

	public static Transcoder getTranscoder() {
		return memClient.getTranscoder();
	}

	private static long getLongValue(Future<Long> f) {
		try {
			Long l = f.get(defaultTimeout, defaultTimeUnit);
			return l.longValue();
		} catch (Exception e) {
			f.cancel(false);
		}
		return -1;
	}

	private static boolean getBooleanValue(Future<Boolean> f) {
		try {
			Boolean bool = f.get(defaultTimeout, defaultTimeUnit);
			return bool.booleanValue();
		} catch (Exception e) {
			f.cancel(false);
			return false;
		}
	}

}