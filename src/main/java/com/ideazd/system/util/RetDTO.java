package com.ideazd.system.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RetDTO implements Serializable {
	private String code = "-1";
	private String msg = "error";
	private Map data = new HashMap();
	
	public RetDTO() {
	}
	
	public RetDTO(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
		if("1".equals(code) && msg.equalsIgnoreCase("error")) {
			this.msg = "sucess";
		}
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map getData() {
		return data;
	}

	public void setData(Map data) {
		this.data = data;
	}
	
	public void put(String key, Object value) {
		getData().put(key, value);
	}
	
	public Object get(String key) {
		return getData().get(key);
	}
}
