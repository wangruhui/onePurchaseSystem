/**
 * 
 */
package com.ideazd.system.util;

import java.util.Map;

/**
 * @author yzz
 * @create 2016年6月30日 上午9:55:56
 */
public interface CheckExcel {
    /**
     * 返回true合法
     *
     * @param data      excel中每一行的数据
     * @return
     */
    public boolean check(Map<String, String> data);
}