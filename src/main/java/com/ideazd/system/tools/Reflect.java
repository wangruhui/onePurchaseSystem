package com.ideazd.system.tools;

import java.lang.reflect.Field;
import java.lang.reflect.Method;



/**
 * 反射工具类
 * @author yzz
 *
 */
public class Reflect {
	/**
	 * 获取属性
	 * @param object
	 * @return
	 */
	public static Field[] getProperty(Object obj){
		try {
			
			Class<?> cls = Class.forName(obj.getClass().getName());
			// 获取属性数组
			Field[] property = cls.getDeclaredFields();
						
			return property;

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 执行set方法
	 * @param object 实体类
	 * @param fieldName 属性名
	 * @param value 
	 */
	public static void invokeSet(Object object,String fieldName,Object value){
		Method [] allMethod = object.getClass().getMethods();
		//根据属性名获取set方法
		Method method = null;
		
		for(int i = 0; i < allMethod.length; i++){
			String methodName = allMethod[i].getName().toUpperCase();
			if(methodName.indexOf("SET"+fieldName.toUpperCase()) > -1){
				method = allMethod[i];
				break;
			}
		}
		try {
			
			if(null != method){
				//执行set方法
				method.invoke(object, new Object[]{value});
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * 执行get方法
	 * @param object 实体类
	 * @param fieldName 属性名
	 * @return
	 */
	public static Object invokeGet(Object object,String fieldName){
        Method [] allMethod = object.getClass().getMethods();
        //根据属性名获取get方法
        Method method = null;
        
		for(int i = 0; i < allMethod.length; i++){
			String methodName = allMethod[i].getName().toUpperCase();
			if(methodName.indexOf("GET" + fieldName.toUpperCase()) > -1){
				method = allMethod[i];
				break;
			}
		}
		
		try {
			//?执行get方法
			return method.invoke(object,new Object[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;
	}
	
}
