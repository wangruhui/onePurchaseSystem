package com.ideazd.system.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model; 
@TableBind(tableName = "sta_new_details",pkName="detid")
public class NewDetails extends Model<NewDetails>{

    private static final long serialVersionUID = 1L;
    public static final NewDetails dao = new NewDetails();
 }