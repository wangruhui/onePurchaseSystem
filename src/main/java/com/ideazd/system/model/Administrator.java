package com.ideazd.system.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model; 
@TableBind(tableName = "administrator",pkName="id")
public class Administrator extends Model<Administrator>{

    private static final long serialVersionUID = 1L;
    public static final Administrator dao = new Administrator();
 }