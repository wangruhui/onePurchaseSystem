package com.ideazd.system.model;

import com.jfinal.ext.plugin.tablebind.TableBind;
import com.jfinal.plugin.activerecord.Model; 
@TableBind(tableName = "sta_propaganda",pkName="proid")
public class Propaganda extends Model<Propaganda>{

    private static final long serialVersionUID = 1L;
    public static final Propaganda dao = new Propaganda();
 }