package com.ideazd.system.msm;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.ideazd.common.utils.HttpClientUtil;
import com.jfinal.kit.JsonKit;

public class SentMessage {


	public static String APP_ID = "393902270000247576";//应用ID------登录平台在应用设置可以找到
	public static String APP_SECRET = "ce9cb08bc2d6c542b32a256f3012309b";//应用secret-----登录平台在应用设置可以找到
	//public static String ACCESS_TOKEN = "";//访问令牌AT-------CC模式，AC模式都可，推荐CC模式获取令牌    -----token会过期不要写死
	public static String URL = "http://api.189.cn/v2/emp/templateSms/sendSms";//验证码通知接口地址---需要保证接口地址在公网能被访问


	
	public static String template_id_one = "91549470";//模板ID，如果有多个短信模板要配置多个
	public static String template_id_two = "91549467";//模板ID，如果有多个短信模板要配置多个
	/**
	 * 重置支付密码验证码
	 * @param telphone
	 * @param verify_code
	 * @return
	 */
	public static String sentVerifyCode(String telphone,String verify_code){
		
		String ret = "";
		String result = "";
		try {
			String accessToken = getAccessToken();
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("projectName", "番斯机");
			paramMap.put("securityCode", verify_code);

			Map<String, Object> sendMap = new HashMap<String, Object>();
			sendMap.put("app_id", APP_ID);
			sendMap.put("access_token", accessToken);
			//sendMap.put("access_token",	Var.get("p2p.message.access_token"));
			sendMap.put("acceptor_tel", telphone);
			sendMap.put("template_id", template_id_one);			
			sendMap.put("template_param", new JsonKit().toJson(paramMap).toString());
			sendMap.put("timestamp", df.format(new Date()));
			String message_url = URL;
			Gson gson = new Gson();
			ret = HttpClientUtil.POSTMethod(message_url, sendMap, null);
			Map<String,String> map=gson.fromJson(ret, new TypeToken<Map<String, String>>() {}.getType());
			System.out.println(ret);
			result=map.get("idertifier").toString();
			System.out.println(result);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 重置密码验证码
	 * @param telphone
	 * @param verify_code
	 * @return
	 */
	public static String sentResetPwd(String telphone,String password){
		
		String ret = "";
		try {
			String accessToken = getAccessToken();
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("projectName", "番斯机");
			paramMap.put("password", password);

			Map<String, Object> sendMap = new HashMap<String, Object>();
			sendMap.put("app_id", APP_ID);
			//sendMap.put("access_token",	Var.get("p2p.message.access_token"));
			sendMap.put("access_token", accessToken);
			sendMap.put("acceptor_tel", telphone);
			sendMap.put("template_id", template_id_two);
			sendMap.put("template_param", new JsonKit().toJson(paramMap).toString());
			sendMap.put("timestamp", df.format(new Date()));
			
			String message_url = URL;
			
			ret = HttpClientUtil.POSTMethod(message_url, sendMap, null);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	
	/**
	 * 获取短信发送令牌
	 * @return
	 */
	public static String getAccessToken(){
		Map<String, Object> send = new HashMap<String, Object>();
		String ret = "";
		try {
			send.put("grant_type", "client_credentials");
			send.put("app_id", APP_ID);
			send.put("app_secret", APP_SECRET);

			ret = HttpClientUtil.POSTMethod("https://oauth.api.189.cn/emp/oauth2/v3/access_token",send, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jo = new JSONObject().parseObject(ret);
	
		return jo.getString("access_token");
	}
}
