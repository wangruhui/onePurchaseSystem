/**
 * 
 */
package com.ideazd.system.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年4月21日 下午4:00:40
 */
@ControllerBind(controllerKey="/login",viewPath="/pages")
@Before(AdminInterceptor.class)
public class LoginController extends Controller {
	private static final Logger log = Logger.getLogger(LoginController.class);
	
	
	//登录
	public void login(){
		String userName = getPara("username");
		String password = getPara("password");
		Administrator admin = Administrator.dao.findFirst("select * from administrator where account = '"+userName+"' and password='"+password+"'");
		
		if(null == admin){
			setAttr("error","用户名或密码有误");
			redirect("/login");
			return;
		}
		
		admin.set("type",SysUtil.getDictionValue("administrator", "type", admin.getStr("type")));
		
		Db.update("update administrator set loginnum=loginnum+1,lastlogin=now(),lastloginip='"+getLocalIp(getRequest())+"' where id = '"+admin.getStr("id")+"'");
		System.out.println(getLocalIp(getRequest()));

		getSession().setAttribute("admin", admin);
		
		Properties props = System.getProperties();
		setSessionAttr("lastLoginDate", admin.get("lastlogin"));
		setSessionAttr("lastLoginIp", admin.get("lastloginip"));
		setSessionAttr("loginnum", admin.get("loginnum"));
		//服务器信息
		setSessionAttr("osName", props.getProperty("os.name"));
		setSessionAttr("useName", props.getProperty("user.name"));
		InetAddress addr = null;
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setSessionAttr("port", getRequest().getLocalPort());
		setSessionAttr("addr", addr.getHostAddress());
		setSessionAttr("filePath", getRequest().getSession().getServletContext().getRealPath("/"));
		setSessionAttr("cpu", Runtime.getRuntime().availableProcessors());
		//setAttr("memory", Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
		
		render("index.jsp");
	}
	
	/**
     * 从Request对象中获得客户端IP，处理了HTTP代理服务器和Nginx的反向代理截取了ip
     * @param request
     * @return ip
     */
    private String getLocalIp(HttpServletRequest request) {
       String remoteAddr = request.getRemoteAddr();
       String forwarded = request.getHeader("X-Forwarded-For");
       String realIp = request.getHeader("X-Real-IP");

       String ip = null;
       if (realIp == null) {
           if (forwarded == null) {
               ip = remoteAddr;
           } else {
               ip = remoteAddr + "/" + forwarded.split(",")[0];
           }
       } else {
           if (realIp.equals(forwarded)) {
               ip = realIp;
           } else {
               if(forwarded != null){
                   forwarded = forwarded.split(",")[0];
               }
               ip = realIp + "/" + forwarded;
           }
       }
       return ip;
   }
}
