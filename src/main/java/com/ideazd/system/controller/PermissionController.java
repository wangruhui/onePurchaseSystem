/**
 * 
 */
package com.ideazd.system.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.Permission;
import com.ideazd.system.model.PermissionAssignment;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/per",viewPath="/pages")
@Before(AdminInterceptor.class)
public class PermissionController extends Controller{
	
	/**
	 * 获取权限列表
	 */
	public void getPermissionList(){
		String curPageStr = getPara("curPage");
		String parentId = getPara("parentId");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
		
		String sql = "from permission where";
		
		if(!StrKit.isBlank(parentId)){
        	sql += " parentid = '"+parentId+"'";
        }else{
        	sql += " parentid is NULL";
        }
		
		try {
			
			Page<Permission> permissionPage = Permission.dao.paginate(curPage, pageSize, "select *", sql+" order by createdate desc");
	        List<Map<Object, Object>> permissionList = new ArrayList<Map<Object, Object>>();
	        
	        for(Permission p:permissionPage.getList()){
	        	Map<Object, Object> permission = new HashMap<Object, Object>();
	        	permission.put("id", p.get("id"));
	        	permission.put("permissionname", p.get("permissionname"));
	        	permission.put("visiturl", p.get("visiturl"));
	        	permission.put("ioc", p.get("ioc"));
	        	permission.put("parentid", p.get("parentid"));
	        	permission.put("createdate", p.get("createdate"));
	        	permissionList.add(permission);
	        }
	        ret.put("data", permissionList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(permissionPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加权限
	 */
	public void addPermission(){
		String permissionName = getPara("permissionName");
		String ioc = getPara("ioc");
		String visitUrl = getPara("visitUrl");
		String parentId = getPara("parentId");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			Permission permission = new Permission();
			permission.set("id", SysUtil.getUUID());
			permission.set("permissionname", permissionName);
			
			
			if(!StrKit.isBlank(parentId)){
				permission.set("parentid", parentId);
			}else{
				permission.set("ioc", ioc);
			}
			permission.set("visiturl", visitUrl);
			permission.set("createid", admin.get("id"));
			permission.set("createdate", new Date());
			permission.set("updateid", admin.get("id"));
			
			if(permission.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑权限
	 */
	public void editPermission(){
		String permissionName = getPara("permissionName");
		String ioc = getPara("ioc");
		String visiturl = getPara("visitUrl");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			Permission permission = new Permission();
			permission.set("id", id);
			permission.set("permissionname", permissionName);
			
			permission.set("ioc", ioc);
			permission.set("visiturl", visiturl);
			permission.set("updateid", admin.get("id"));
			
			if(permission.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	//根据ID获取权限详细
	public void findPermissionById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			Permission permission = Permission.dao.findById(id);
			ret.put("permissionName", permission.get("permissionname"));
			ret.put("visitUrl", permission.get("visiturl"));
			ret.put("ioc", permission.get("ioc"));
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除权限
	 */
	@SuppressWarnings("null")
	public void delPermission(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from permission where id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 用户添加权限
	 */
	 public void addPermissionByAdmin(){
		String id = getPara("id");
		String ids = getPara("ids");
		
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			Db.update("update permissionassignment set permissionid = '"+ids+"',updateid='"+admin.get("id")+"' where adminid='"+id+"'");
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}

}
