/**
 * 
 */
package com.ideazd.system.controller;

import java.util.List;

import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Permission;
import com.ideazd.system.model.PermissionAssignment;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

/**
 * @author yzz
 * @create 2016年4月21日 下午4:00:40
 */
@ControllerBind(controllerKey="/",viewPath="/pages")
@Before(AdminInterceptor.class)
public class ForwardPageController extends Controller {
	private static final Logger log = Logger.getLogger(ForwardPageController.class);
	
	//登录
	public void login(){
		String error = getPara("error");
		String ip = getPara("ip");
		
		if(!StrKit.isBlank(error)){
			
			if("exist".equals(error)){
				setAttr("error","此管理员已在ip:"+ip+" 登录！");
			}else{
				setAttr("error","用户名或密码有误");
			}
			
		}
		render("login.jsp");
	}
	//首页
	public void index(){
		render("index.jsp");
	}
	
	//资讯
	public void newsdetailList(){
		render("newsdetail-list.jsp");
	}
	
	//添加资讯
	public void newsdetailAdd(){
		render("newsdetail-add.jsp");
	}
	
	//添加测试资讯
		public void testdetailAdd(){
			render("test-add.jsp");
		}
	
	//编辑资讯
	public void newsdetailEdit(){
		String id = getPara("id");
		render("newsdetail-add.jsp?id="+id);
	}
	public void testdetailEdit(){
		String id = getPara("id");
		render("test-add.jsp?id="+id);
	}
	
	//话题
	public void topicList(){
		render("topic-list.jsp");
	}
	
	//编辑话题
	public void topicEdit(){
		String id = getPara("id");
		render("topic-add.jsp?id="+id);
	}
	
	//查看话题评论
	public void showTopicComment(){
		String topicId = getPara("topicId");
		render("topic-comment-list.jsp?topicId="+topicId);
	}
	
	//查看评论
	public void topiCommentList(){
		render("topic-comment-list.jsp");
	}
	
	//商城
	public void commodityList(){
		render("commodity-list.jsp");
	}
	
	//添加商城
	public void commodityAdd(){
		String id = getPara("id");
		render("commodity-add.jsp?id="+id);
	}
	
	//商品型号
	public void commodityVersionList(){
		String commodityId = getPara("commodityId");
		render("commodity-version-list.jsp?commodityId="+commodityId);
	}
	
	//添加商品型号
	public void commodityVersionAdd(){
		String commodityId = getPara("commodityId");
		String versionId = getPara("versionId");
		render("commodity-version-edit.jsp?commodityId="+commodityId+"&versionId="+versionId);
	}
	
	//添加商品型号图片
	public void uploadPic(){
		String id = getPara("id");
		render("uploadfile.jsp?id=="+id);
	}
	
	public void getVersionPicList(){
		String id = getPara("id");
		render("commodity-version-pic-list.jsp?id=="+id);
	}
	
	//商品订单
	public void getCommOrderList(){
		render("order-list.jsp");
	}
	
	//发货
	public void expressEdit(){
		String id = getPara("id");
		render("express-edit.jsp?id="+id);
	}
	
	//发货
	public void setRefund(){
		String id = getPara("id");
		render("refundfinish.jsp?id="+id);
	}

	//运费列表
	public void getCostList(){
		render("cost-list.jsp");
	}
	
	
	//添加运费
	public void costAdd(){
		String id = getPara("id");
		render("cost-add.jsp?id="+id);
	}
	
	//会员管理
	public void memberList(){
		render("member-list.jsp");
	}
	
	//界面排版
	public void interfaceList(){
		render("interface-list.jsp");
	}
	
	//界面排版编辑
	public void interfaceEdit(){
		String id = getPara("id");
		render("interface-edit.jsp?id="+id);
	}
	
	//入帐列表 
	public void getAccountIn(){
		render("accounted-in.jsp");
	}
	
	//出帐列表 
	public void getAccountOut(){
		render("accounted-out.jsp");
	}
	
	//宣传管理
	public void propagandaList(){
		render("propaganda-list.jsp");
	}
	
	//宣传管理编辑
	public void propagandaEdit(){
		String id = getPara("id");
		render("propaganda-add.jsp?id="+id);
	}
	
	
	//意见反馈
	public void feedbackList(){
		render("feedback-list.jsp");
	}
	
	//关键词
	public void keywordList(){
		render("keyword-list.jsp");
	}
	
	public void keywordExcel(){
		render("keyword-excel.jsp");
	}
	
	//编辑关键词
	public void keywordEdit(){
		String id = getPara("id");
		render("keyword-add.jsp?id="+id);
	}
	
	//管理员列表
	public void adminList(){
		render("admin-list.jsp");
	}
	
	//管理员编辑
	public void adminAdd(){
		String id = getPara("id");
		render("admin-add.jsp?id="+id);
	}
	
	//权限列表
	public void getPermissionList(){
		render("permission-list.jsp");
	}
	
	//权限列表
	public void addPermission(){
		String id = getPara("id");
		render("permission-edit.jsp?id="+id);
	}
	
	
	//子权限列表
	public void getChildPermission(){
		String id = getPara("id");
		render("permission-child-list.jsp?id="+id);
	}
	
	//子权限列表
	public void getAssignment(){
		String id = getPara("id");
		List<Permission> permissionList = Permission.dao.find("select * from permission where parentid is NULL");
		
		PermissionAssignment permissionassignment = PermissionAssignment.dao.findFirst("select * from permissionassignment where adminid = '"+id+"'");
		
		String perId = "";
		
		if(permissionassignment != null){
			if(permissionassignment.getStr("permissionid").indexOf(",")>-1){
				String perIds[] = permissionassignment.getStr("permissionid").split(",");
				
				for(String s:perIds){
					perId +="'"+s+"',";
				}
				perId = perId.substring(0,perId.length()-1);
			}else{
				perId = permissionassignment.getStr("permissionid");
			}
		}
		
		if(""!=perId){
			
			for(int i = 0;i<permissionList.size();i++){
				System.out.println(permissionList.get(i).getStr("id"));
				System.out.println(perId);
				if(perId.indexOf(permissionList.get(i).getStr("id"))>-1){
					permissionList.get(i).set("parentid", "checked");
				}
			}
		}
		
		
		setAttr("permissionList", permissionList);
		setAttr("adminId", id);
		render("assignment.jsp");
	}
	
	//权限列表
	public void addChildPermission(){
		String id = getPara("id");
		String parentId = getPara("parentId");
		render("permission-child-edit.jsp?parentId="+parentId+"&id="+id);
	}
	
	//系统设置
	public void systemBase(){
		render("system-base.jsp");
	}
	
	//数据字典
	public void systemData(){
		render("system-data.jsp");
	}
	
	//数据字典
	public void systemDataList(){
		String dictionaryId = getPara("dictionaryId");
		render("system-data-list.jsp?dictionaryId="+dictionaryId);
	}
	
	//数据字典
	public void systemDataEdit(){
		String datadictionId = getPara("datadictionId");
		String dictionaryId = getPara("dictionaryId");
		render("system-data-edit.jsp?datadictionId="+datadictionId+"&dictionaryId="+dictionaryId);
	}
	//测试
	public void testMethod(){
		render("test.jsp");
	}	
}
