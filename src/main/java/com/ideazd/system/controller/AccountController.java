/**
 * 
 */
package com.ideazd.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.AccountedIn;
import com.ideazd.system.model.AccountedOut;
import com.ideazd.system.tools.SysUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年4月21日 下午4:00:40
 */
@ControllerBind(controllerKey="/account")
@Before(AdminInterceptor.class)
public class AccountController extends Controller {
	private static final Logger log = Logger.getLogger(AccountController.class);
	
	/**
	 * 入帐列表
	 */
	public void getAccountInList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		Map<Object,Object> ret = new HashMap<Object,Object>();
        
        if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
        
        String sql = "from sta_accounted_in a left join sta_user u on a.useid = u.useid";
        
        Page<AccountedIn> accountedInPage = AccountedIn.dao.paginate(curPage, pageSize, "select a.*,u.nickname", sql+" order by a.createdate desc");
        List<Map<String, String>> accountedInList = new ArrayList<Map<String, String>>();
        
        for(AccountedIn a:accountedInPage.getList()){
        	Map<String, String> accountedIn = new HashMap<String, String>();
        	accountedIn.put("id", a.getStr("accinid"));
        	accountedIn.put("nickname", a.getStr("nickname"));
        	accountedIn.put("ordnumber", a.getStr("ordnumber"));
        	accountedIn.put("payorderid", a.getStr("payorderid"));
        	accountedIn.put("intime", a.get("intime").toString().toString().substring(0,a.get("intime").toString().toString().length()-2));
        	accountedIn.put("inamount", a.getStr("inamount"));
        	accountedIn.put("intype", SysUtil.getDictionValue("sta_order_summary", "paytype", a.getStr("intype").toString()));
        	accountedInList.add(accountedIn);
        }
        ret.put("data", accountedInList);
        
        PagingUtil pagingUtil = new PagingUtil();
        pagingUtil.setRowCount(accountedInPage.getTotalRow());
        pagingUtil.setCurrentPage(curPage);
        
        
        ret.put("pages", pagingUtil.getPageDisplay());
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 出帐记录
	 */
	public void getAccountedOutList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		Map<Object,Object> ret = new HashMap<Object,Object>();
        
        if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
        
        String sql = "from sta_accounted_out a left join sta_user u on a.useid = u.useid";
        
        Page<AccountedOut> accountedOutPage = AccountedOut.dao.paginate(curPage, pageSize, "select a.*,u.nickname", sql+" order by a.createdate desc");
        List<Map<Object, Object>> accountedOutList = new ArrayList<Map<Object, Object>>();
        
        for(AccountedOut a:accountedOutPage.getList()){
        	Map<Object, Object> accountedOut = new HashMap<Object, Object>();
        	accountedOut.put("id", a.getStr("accoutid"));
        	accountedOut.put("nickname", a.getStr("nickname"));
        	accountedOut.put("ordnumber", a.getStr("ordnumber"));
        	accountedOut.put("outtype", SysUtil.getDictionValue("sta_accounted_out", "outtype", a.getStr("outtype")));
        	accountedOut.put("payorderid", a.get("payorderid").toString());
        	accountedOut.put("outtime", a.get("outtime"));
        	accountedOut.put("outamount", a.get("outamount"));
        	accountedOut.put("bankaccount", a.get("bankaccount"));
        	accountedOut.put("accountname", a.get("accountname"));
        	accountedOut.put("cardnumber", a.get("cardnumber"));
        	accountedOut.put("outremarks", a.getStr("outremarks"));
        	accountedOut.put("updatename", a.getStr("updatename"));
        	accountedOutList.add(accountedOut);
        }
        ret.put("data", accountedOutList);
        
        PagingUtil pagingUtil = new PagingUtil();
        pagingUtil.setRowCount(accountedOutPage.getTotalRow());
        pagingUtil.setCurrentPage(curPage);
        
        
        ret.put("pages", pagingUtil.getPageDisplay());
		
		renderJson(ret);
		return;
	}
}
