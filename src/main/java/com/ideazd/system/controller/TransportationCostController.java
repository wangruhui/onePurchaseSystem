/**
 * 
 */
package com.ideazd.system.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.SysAreas;
import com.ideazd.system.model.TransportationCost;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/cost",viewPath="/pages")
@Before(AdminInterceptor.class)
public class TransportationCostController extends Controller{
	
	/**
	 * 运费列表
	 */
	public void getCostList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_transportation_cost c left join sysareas s on c.provincecode = s.area_code  where 1 = 1 and c.provincecode != '' order by c.updatedate";
		
		try {
			
			Page<TransportationCost> transportationCostPage = TransportationCost.dao.paginate(curPage, pageSize, "select c.*,s.area_name", sql);
	        List<Map<Object, Object>> transportationCostList = new ArrayList<Map<Object, Object>>();
	        
	        for(TransportationCost t:transportationCostPage.getList()){
	        	Map<Object, Object> transportationCost = new HashMap<Object, Object>();
	        	transportationCost.put("id", t.get("cosid"));
	        	transportationCost.put("areaName",  t.get("area_name"));
	        	transportationCost.put("cost", t.get("cost"));
	        	transportationCost.put("createTime", t.get("createtime"));
	        	transportationCost.put("createName", t.get("createname"));
	        	transportationCost.put("updateName", t.get("updatename"));
	        	transportationCost.put("updateDate", t.get("updatedate"));
	        	transportationCostList.add(transportationCost);
	        }
	        ret.put("data", transportationCostList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(transportationCostPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加运费
	 */
	public void addCost(){
		String provinceCode = getPara("provinceCode");
		String cost = getPara("cost");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			TransportationCost transportationCost = new TransportationCost();
			transportationCost.set("cosid", SysUtil.getUUID());
			transportationCost.set("provincecode", provinceCode);
			transportationCost.set("cost", cost);
			transportationCost.set("createid", admin.get("id"));
			transportationCost.set("createname", admin.get("name"));
			transportationCost.set("createtime", new Date());
			transportationCost.set("updateid", admin.get("id"));
			transportationCost.set("updatename", admin.get("name"));
			transportationCost.set("updatedate", new Date());
			transportationCost.set("remarks", "添加运费");
			
			if(transportationCost.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑运费
	 */
	public void editCost(){
		String provinceCode = getPara("provinceCode");
		String cost = getPara("cost");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			TransportationCost transportationCost = new TransportationCost();
			transportationCost.set("cosid", id);
			transportationCost.set("provincecode", provinceCode);
			transportationCost.set("cost", cost);
			transportationCost.set("updateid", admin.get("id"));
			transportationCost.set("updatename", admin.get("name"));
			transportationCost.set("updatedate", new Date());
			transportationCost.set("remarks", "添加运费");
			
			if(transportationCost.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除商品
	 */
	@SuppressWarnings("null")
	public void delCost(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_transportation_cost where cosid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/*
	 * 获取默认运费
	 */
	public void getDefaultCost(){
		RetDTO ret = new RetDTO();
		
		try {
			
			TransportationCost transportationCost = TransportationCost.dao.findById("3427dc37874a40e38f70f56e984471cs");
			ret.put("cost", transportationCost.get("cost"));
			ret.setCode("0");
			ret.setMsg("获取成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	
	/**
	 * 修改默认运费
	 */
	public void editDefaultCost(){
		String cost = getPara("cost");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			TransportationCost transportationCost = new TransportationCost();
			transportationCost.set("cosid", "3427dc37874a40e38f70f56e984471cs");
			transportationCost.set("cost", cost);
			transportationCost.set("updateid", admin.get("id"));
			transportationCost.set("updatename", admin.get("name"));
			transportationCost.set("updatedate", new Date());
			transportationCost.set("remarks", "修改默认运费");
			
			if(transportationCost.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取省
	 */
	public void getProvice(){
		Map<Object, Object> ret = new HashMap<Object, Object>();
		
		try {
			
			List<SysAreas> sysAreas = SysAreas.dao.find("select * from sysareas where area_parent = '0'");
			List<Map<Object, Object>> areasList = new ArrayList<Map<Object, Object>>();
	        	
			for(SysAreas s:sysAreas){
				Map<Object, Object> areas = new HashMap<Object, Object>();
				areas.put("areaCode", s.get("area_code"));
				areas.put("areaName", s.get("area_name"));
				areasList.add(areas);
			}
			
			ret.put("code", "0");
			ret.put("msg", "成功");
			ret.put("data", areasList);
			
		} catch (Exception e) {
			ret.put("code", "-1");
			ret.put("msg", "系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/*
	 * 根据id获取运费
	 */
	public void getCostById(){
		String id = getPara("id");
		Map<Object, Object> ret = new HashMap<Object, Object>();
		
		try {
			
			TransportationCost transportationCost = TransportationCost.dao.findById(id);
			Map<Object, Object> cost = new HashMap<Object, Object>();
			cost.put("provinceCode", transportationCost.get("provincecode"));
			System.out.println(transportationCost.get("provincecode"));
			cost.put("cost", transportationCost.get("cost"));
			
			ret.put("code", "0");
			ret.put("msg", "成功");
			ret.put("data",cost);
			
		} catch (Exception e) {
			ret.put("code", "-1");
			ret.put("msg", "系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 区域验证
	 */
	public void provinceValidate(){
		String province = getPara("province");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			String sql = "select * from sta_transportation_cost where provincecode = '"+province+"'";
			
			if(!StrKit.isBlank(id)){
				sql += " and cosid != '"+id+"'";
			}
			
			TransportationCost tcCost = TransportationCost.dao.findFirst(sql);
			
			if(null != tcCost){
				ret.setCode("-2");
				ret.setMsg("已存在");
			}else{
				ret.setCode("0");
				ret.setMsg("可以添加");
			}
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}

}
