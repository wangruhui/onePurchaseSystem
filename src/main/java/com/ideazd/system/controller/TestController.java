/**
 * 
 */
package com.ideazd.system.controller;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.FeedBack;
import com.ideazd.system.model.NewDetails;
import com.ideazd.system.model.Propaganda;
import com.ideazd.system.model.TestTable;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.ideazd.system.util.UmengMsg;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/test",viewPath="/pages")
@Before(AdminInterceptor.class)
public class TestController extends Controller{
	
	/**
	 * 获取资讯列表
	 */
	public void getNewsDetailList(){
		String curPageStr = getPara("curPage");
		String title = getPara("title");
		String publishDate = getPara("publishDate");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from test_table where 1 = 1";
        
        if(!StrKit.isBlank(title)){
        	sql += " and dettitle like '"+title+"'";
        }
        
        if(!StrKit.isBlank(publishDate)){
        	sql += " and createtime >'"+publishDate+"' and createtime < "+ "'"+publishDate.substring(0,10)+" 23:59:59'";
        }
		
		try {
			
			Page<TestTable> NewDetailslPage = TestTable.dao.paginate(curPage, pageSize, "select *", sql+" order by updatedate desc");
	        List<Map<String, String>> NewDetailstList = new ArrayList<Map<String, String>>();
	        
	        for(TestTable n:NewDetailslPage.getList()){
	        	Map<String, String> NewDetails = new HashMap<String, String>();
	        	NewDetails.put("id", n.getStr("id"));
	        	NewDetails.put("dettitle", n.getStr("dettitle"));
	        	NewDetails.put("detcontenturl", n.getStr("detcontenturl"));
	        	NewDetails.put("detcontent", n.getStr("detcontent"));
	        	NewDetails.put("createtime", n.get("createdate").toString().substring(0,n.get("createdate").toString().length()-2));
	        	NewDetails.put("displayorder", n.get("displayorder").toString());
	        	NewDetails.put("createname", n.getStr("createname"));
	        	NewDetails.put("updatename", n.getStr("updatename"));
	        	NewDetailstList.add(NewDetails);
	        }
	        ret.put("data", NewDetailstList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(NewDetailslPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加资讯
	 */
	public void addNewsDetail(){
		String dettitle = getPara("dettitle");
		String detcontent = getPara("detcontent");
		String createtime = getPara("createtime");
		String displayorder = getPara("displayorder");
		RetDTO ret = new RetDTO();
		try {
			
			String uuid = SysUtil.getUUID();
			Administrator admin = getSessionAttr("admin");
			
			TestTable newDetails = new TestTable();
			newDetails.set("id", uuid);
			newDetails.set("dettitle", dettitle);
			newDetails.set("detcontenturl", getRequest().getScheme()+"://"+getRequest().getServerName()+":"+getRequest().getServerPort()+getRequest().getContextPath()+"/news/showNews?id="+uuid);
			newDetails.set("detcontent", detcontent);
			if(StrKit.isBlank(createtime)){
				newDetails.set("createtime", new Date());
			}else{
				newDetails.set("createtime", createtime);
			}
			
			newDetails.set("displayorder", displayorder);
			newDetails.set("createid", admin.get("id"));
			newDetails.set("createname", admin.get("name"));
			newDetails.set("createdate", new Date());
			newDetails.set("updateid", admin.get("id"));
			newDetails.set("updatename", admin.get("name"));
			newDetails.set("updatedate", new Date());
			newDetails.set("remarks", "添加资讯");
			
			if(newDetails.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
		
		
	}
	
	/**
	 * 编辑资讯
	 */
	public void editNewsDetail(){
		String id = getPara("id");
		String dettitle = getPara("dettitle");
		String detcontent = getPara("detcontent");
		String createtime = getPara("createtime");
		String displayorder = getPara("displayorder");
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			TestTable newDetails = new TestTable();
			newDetails.set("id", id);
			newDetails.set("dettitle", dettitle);
			newDetails.set("detcontent", detcontent);
			if(StrKit.isBlank(createtime)){
				newDetails.set("createtime", new Date());
			}else{
				newDetails.set("createtime", createtime);
			}
			newDetails.set("displayorder", displayorder);
			newDetails.set("updateid", admin.get("id"));
			newDetails.set("updatename", admin.get("name"));
			newDetails.set("updatedate", new Date());
			newDetails.set("remarks", "添加资讯");
			
			if(newDetails.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	//根据ID获资讯详细
	public void findNewsById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			
			TestTable newDetails = TestTable.dao.findById(id);
			ret.put("id", newDetails.getStr("id"));
			ret.put("dettitle", newDetails.getStr("dettitle"));
			ret.put("detcontenturl", newDetails.getStr("detcontenturl"));
			ret.put("detcontent", newDetails.getStr("detcontent"));
			ret.put("createtime", newDetails.get("createtime").toString());
			ret.put("displayorder", newDetails.get("displayorder").toString());
			
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除界面排版
	 */
	@SuppressWarnings("null")
	public void delNewsDetail(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from test_table where detid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	

	/**
	 * 查看资讯
	 */
	public void showNews(){
		String id = getPara("id");
		try {
			TestTable newDetail = TestTable.dao.findById(id);
			System.out.println(newDetail.get("createdate"));
			setAttr("newDetail", newDetail);
			render("newsshow.jsp");
		} catch (Exception e) {
			System.out.print(e);
		}
		
	}
	


}
