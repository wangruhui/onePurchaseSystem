/**
 * 
 */
package com.ideazd.system.controller;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.FeedBack;
import com.ideazd.system.model.InterfaceLayout;
import com.ideazd.system.model.KeyWord;
import com.ideazd.system.model.NewDetails;
import com.ideazd.system.model.Propaganda;
import com.ideazd.system.model.Topic;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.ExcelUtils;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/basic",viewPath="/pages")
@Before(AdminInterceptor.class)
public class BasicController extends Controller{
	
	/**
	 * 获取界面排版列表
	 */
	public void getInterfaceList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
		
		try {
			
			Page<InterfaceLayout> interfaceLayoutPage = InterfaceLayout.dao.paginate(curPage, pageSize, "select *", "from sta_interface_layout order by updatedate desc");
	        List<Map<String, String>> InterfaceLayoutList = new ArrayList<Map<String, String>>();
	        
	        for(InterfaceLayout i:interfaceLayoutPage.getList()){
	        	Map<String, String> interfaceLayout = new HashMap<String, String>();
	        	interfaceLayout.put("id", i.getStr("layid"));
	        	interfaceLayout.put("interfacename", SysUtil.getDictionValue("sta_interface_layout", "interfacename", i.getStr("interfacename")));
	        	interfaceLayout.put("interfaceurl", i.getStr("interfaceurl"));
	        	interfaceLayout.put("createname", i.getStr("createname"));
	        	interfaceLayout.put("createdate", i.get("createdate").toString().substring(0,i.get("createdate").toString().length()-2));
	        	interfaceLayout.put("updatename", i.getStr("updatename"));
	        	interfaceLayout.put("updatedate", i.get("updatedate").toString());
	        	InterfaceLayoutList.add(interfaceLayout);
	        }
	        ret.put("data", InterfaceLayoutList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(interfaceLayoutPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加界面排版
	 */
	public void addInterface(){
		String interfaceName = getPara("interfaceName");
		String interfaceURL = getPara("interfaceURL");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			InterfaceLayout interfaceLayout = new InterfaceLayout();
			interfaceLayout.set("layid", SysUtil.getUUID());
			interfaceLayout.set("interfacename", interfaceName);
			interfaceLayout.set("interfaceurl", interfaceURL);
			interfaceLayout.set("createid", admin.get("id"));
			interfaceLayout.set("createname", admin.get("name"));
			interfaceLayout.set("createdate", new Date());
			interfaceLayout.set("updateid", admin.get("id"));
			interfaceLayout.set("updatename", admin.get("name"));
			interfaceLayout.set("updatedate", new Date());
			interfaceLayout.set("remarks", "添加成功");
			
			if(interfaceLayout.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑界面排版
	 */
	public void editInterface(){
		String interfaceName = getPara("interfaceName");
		String interfaceURL = getPara("interfaceURL");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = getSessionAttr("admin");
			
			InterfaceLayout interfaceLayout = new InterfaceLayout();
			interfaceLayout.set("layid", id);
			interfaceLayout.set("interfacename", interfaceName);
			interfaceLayout.set("interfaceurl", interfaceURL);
			interfaceLayout.set("updateid", admin.get("id"));
			interfaceLayout.set("updatename", admin.get("name"));
			interfaceLayout.set("updatedate", new Date());
			interfaceLayout.set("remarks", "更新成功");
			
			if(interfaceLayout.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	//根据ID获取界面排版
	public void findInterfaceById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			
			InterfaceLayout interfaceLayout = InterfaceLayout.dao.findById(id);
			ret.put("interfacename", interfaceLayout.get("interfacename"));
			ret.put("interfaceurl", interfaceLayout.get("interfaceurl"));
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除界面排版
	 */
	@SuppressWarnings("null")
	public void delInterface(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_interface_layout where layid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取意见反馈列表
	 */
	@SuppressWarnings("deprecation")
	public void getFeedBackList(){
		String handleState = getPara("handleState");
		String submitDate = getPara("submitDate");
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
		
		String sql = "from sta_feedback f left join sta_user u on f.useid = u.useid where 1=1";
        
        if(!StrKit.isBlank(handleState)){
        	sql += " and f.handlestate = '"+handleState+"'";
        }
        
        if(!StrKit.isBlank(submitDate)){
        	sql += "and f.createtime >'"+submitDate+"' and f.createtime < "+ "'"+submitDate.substring(0,10)+" 23:59:59'";
        }
        
        
		try {
			
			Page<FeedBack> feedbackPage = FeedBack.dao.paginate(curPage, pageSize, "select f.*,u.nickname", sql+" order by f.createtime desc");
	        List<Map<String, String>> feedbackList = new ArrayList<Map<String, String>>();
	        
	        for(FeedBack f:feedbackPage.getList()){
	        	Map<String, String> feedback = new HashMap<String, String>();
	        	feedback.put("id", f.getStr("feeid"));
	        	feedback.put("nickname", URLDecoder.decode(f.getStr("nickname")));
	        	feedback.put("feeinfo", URLDecoder.decode(f.getStr("feeinfo")));
	        	feedback.put("contactmode", URLDecoder.decode(f.getStr("contactmode")));
	        	feedback.put("handlestate", f.get("handlestate").toString());
	        	feedback.put("createtime", f.get("createtime").toString().substring(0,f.get("createtime").toString().length()-2));
	        	feedbackList.add(feedback);
	        }
	        ret.put("data", feedbackList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(feedbackPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 处理意见反馈
	 */
	public void handleFeedBack(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			FeedBack feedback = new FeedBack();
			feedback.set("feeid", id);
			feedback.set("handlestate", "1");
			
			if(feedback.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取宣传管理列表
	 */
	public void getPropagandaList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
		
		try {
			
			Page<Propaganda> propagandaPage = Propaganda.dao.paginate(curPage, pageSize, "select *", "from sta_propaganda order by update_date desc");
	        List<Map<String, Object>> propagandaList = new ArrayList<Map<String, Object>>();
	        
	        for(Propaganda p:propagandaPage.getList()){
	        	Map<String, Object> propaganda = new HashMap<String, Object>();
	        	propaganda.put("id", p.getStr("proid"));
	        	propaganda.put("picposition", SysUtil.getDictionValue("sta_interface_layout", "interfacename", p.getStr("picposition")));
	        	propaganda.put("picaddress", p.getStr("picaddress"));
	        	propaganda.put("displayorder", p.getStr("displayorder"));
	        	propaganda.put("displaystatus", p.getStr("displaystatus"));
	        	propaganda.put("operationevent", SysUtil.getDictionValue("sta_propaganda", "operationevent", p.getStr("operationevent")));
	        	
	        	if(p.getStr("operationevent").equals("2")){
	        		Topic topic = Topic.dao.findById(p.getStr("eventinfo"));
	        		propaganda.put("eventinfo", topic.getStr("toptitle"));
	        	}else if(p.getStr("operationevent").equals("3")){
	        		NewDetails newDetails = NewDetails.dao.findById(p.getStr("eventinfo"));
	        		propaganda.put("eventinfo", newDetails.getStr("dettitle"));
	        	}else{
	        		propaganda.put("eventinfo", p.getStr("eventinfo"));
	        	}
	        	
	        	propaganda.put("updatedate", p.get("update_date").toString().substring(0,p.get("update_date").toString().length()-2));
	        	propaganda.put("createname", p.get("create_name").toString());
	        	propaganda.put("updatename", p.get("update_name").toString());
	        	propagandaList.add(propaganda);
	        }
	        ret.put("data", propagandaList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(propagandaPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加宣传
	 */
	public void addPropaganda(){
		File file = super.getFile("picaddress").getFile();
		String picposition = getPara("picposition");
		String displayorder = getPara("displayorder");
		String displaystatus = getPara("displaystatus");
		String operationevent = getPara("operationevent");
		String eventinfo = getPara("eventinfo");
		String h5event = getPara("h5event");
		RetDTO ret = new RetDTO();
		String picaddress = "";
		
		if(operationevent.equals("1")){
			eventinfo = h5event;
		}
		
		try {
			
			String uuid = SysUtil.getUUID();
			
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/propaganda" + "/" + uuid + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				picaddress = imaPath.get("serverPath");

			}
			
			Administrator admin = getSessionAttr("admin");
			
			Propaganda propaganda = new Propaganda();
			propaganda.set("proid", uuid);
			propaganda.set("picposition", picposition);
			
			if(!StrKit.isBlank(picaddress)){
                 propaganda.set("picaddress", picaddress);
			}
			
			propaganda.set("displayorder", displayorder);
			propaganda.set("displaystatus", displaystatus);
			propaganda.set("operationevent", operationevent);
			propaganda.set("eventinfo", eventinfo);
			propaganda.set("create_by", admin.get("id"));
			propaganda.set("create_name", admin.get("name"));
			propaganda.set("create_date", new Date());
			propaganda.set("update_by", admin.get("id"));
			propaganda.set("update_name", admin.get("name"));
			propaganda.set("update_date", new Date());
			propaganda.set("remarks", "修改成功");
			
			if(propaganda.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑宣传
	 */
	public void editPropaganda(){
		File file = null;
		
		try {
			file = super.getFile("picaddress").getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			file = null;
		}
		String id = getPara("id");
		String picposition = getPara("picposition");
		String displayorder = getPara("displayorder");
		String displaystatus = getPara("displaystatus");
		String operationevent = getPara("operationevent");
		String h5event = getPara("h5event");
		String eventinfo = getPara("eventinfo");
		RetDTO ret = new RetDTO();
		String picaddress = "";
		
		if(operationevent.equals("1")){
			eventinfo = h5event;
		}
		
		try {
			
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/propaganda" + "/" + id + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				picaddress = imaPath.get("serverPath");

			}
			
			Administrator admin = getSessionAttr("admin");
			
			Propaganda propaganda = new Propaganda();
			propaganda.set("proid", id);
			propaganda.set("picposition", picposition);
			
			if(!StrKit.isBlank(picaddress)){
				propaganda.set("picaddress", picaddress);
			}
			
			propaganda.set("displayorder", displayorder);
			propaganda.set("displaystatus", displaystatus);
			propaganda.set("operationevent", operationevent);
			propaganda.set("eventinfo", eventinfo);
			propaganda.set("create_by", admin.get("id"));
			propaganda.set("create_name", admin.get("name"));
			propaganda.set("create_date", new Date());
			propaganda.set("update_by", admin.get("id"));
			propaganda.set("update_name", admin.get("name"));
			propaganda.set("update_date", new Date());
			propaganda.set("remarks", "修改成功");
			
			if(propaganda.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	//根据ID获取宣传详细
	public void findPropagandaById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			
			Propaganda propaganda = Propaganda.dao.findById(id);
			ret.put("id", propaganda.getStr("proid"));
			ret.put("picposition", propaganda.get("picposition").toString());
			ret.put("picaddress", propaganda.getStr("picaddress"));
			ret.put("displayorder", propaganda.getStr("displayorder"));
			ret.put("displaystatus", propaganda.getStr("displaystatus"));
			ret.put("operationevent", propaganda.getStr("operationevent"));
			ret.put("eventinfo", propaganda.getStr("eventinfo"));
			
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除宣传
	 */
	@SuppressWarnings("null")
	public void delPropaganda(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_propaganda where proid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 启用/禁用
	 */
	public void handleDisplay(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			
			Propaganda propaganda = new Propaganda();
			propaganda.set("proid", id);
			propaganda.set("displaystatus", status);
			propaganda.set("update_by", admin.get("id"));
			propaganda.set("update_name", admin.get("name"));
			propaganda.set("update_date", new Date());
			
			if(propaganda.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取话题
	 */
	public void getTopicList(){
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		try {
			
			List<Topic> topicList = Topic.dao.find("select * from sta_topic where displaystatus = '0'");
	        List<Map<String, Object>> topics = new ArrayList<Map<String, Object>>();
	        
	        for(Topic p:topicList){
	        	Map<String, Object> topic = new HashMap<String, Object>();
	        	topic.put("id", p.getStr("topid"));
	        	topic.put("toptitle", p.getStr("toptitle"));
	        	topics.add(topic);
	        }
	        ret.put("data", topics);
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取资讯
	 */
	public void getNewsList(){
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		try {
			
			List<NewDetails> newsList = NewDetails.dao.find("select * from sta_new_details where displaystatus = '0'");
	        List<Map<String, Object>> news = new ArrayList<Map<String, Object>>();
	        
	        for(NewDetails n:newsList){
	        	Map<String, Object> newdetail = new HashMap<String, Object>();
	        	newdetail.put("id", n.getStr("detid"));
	        	newdetail.put("dettitle", n.getStr("dettitle"));
	        	news.add(newdetail);
	        }
	        ret.put("data", news);
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}

	/**
	 * 获取关键字列表
	 */
	public void getKeyWordList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
		
		
		try {
			
			Page<KeyWord> keyWordPage = KeyWord.dao.paginate(curPage, pageSize, "select *", " from keyword order by createdate desc");
			PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(keyWordPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("data", keyWordPage.getList());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	/**
	 * 根据获取关键字
	 */
	public void getKeyWordById(){
		String id = getPara("id");
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		
		try {
			
			KeyWord keyWord = KeyWord.dao.findById(id);;
	        
	        
	        ret.put("data", keyWord);
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加关键字
	 */
	public void addKeyword(){
		String word = getPara("word");
		String enable = getPara("enable");
		Administrator admin = getSessionAttr("admin");
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		try {
			
			KeyWord keyword = new KeyWord();
			keyword.set("id", SysUtil.getUUID());
			keyword.set("word", word);
			keyword.set("enable", enable);
			keyword.set("createdate", new Date());
			keyword.set("updateid", admin.get("id"));
			
			if(keyword.save()){
				ret.put("code", "0");
		        ret.put("msg", "添加成功");
			}
	        
	        
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
		
	}
	
	/**
	 * 修改关键字
	 */
	public void editKeyword(){
		String word = getPara("word");
		String enable = getPara("enable");
		String id = getPara("id");
		Administrator admin = getSessionAttr("admin");
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		try {
			
			KeyWord keyword = new KeyWord();
			keyword.set("id", id);
			keyword.set("word", word);
			keyword.set("enable", enable);
			keyword.set("updateid", admin.get("id"));
			
			if(keyword.update()){
				ret.put("code", "0");
		        ret.put("msg", "修改成功");
			}
	        
	        
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
		
	}
	
	/**
	 * 关键词 启用/禁用
	 */
	public void handleKeyword(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			
			KeyWord keyword = new KeyWord();
			keyword.set("id", id);
			keyword.set("enable", status);
			keyword.set("updateid", admin.get("id"));
			
			if(keyword.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除关键词
	 */
	@SuppressWarnings("null")
	public void delKeyword(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from keyword where id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	public void uploadExcel(){
	  RetDTO ret = new RetDTO();
      File file = super.getFile("file").getFile();
      System.out.println(file.getName());
      System.out.println(file.getPath());
      
      try {
			
    	  XSSFWorkbook xssfWorkbook = new XSSFWorkbook(file.getPath());    
          Administrator admin = getSessionAttr("admin");
          // 循环工作表Sheet    
          for(int numSheet = 0; numSheet < xssfWorkbook.getNumberOfSheets(); numSheet++){    
            XSSFSheet xssfSheet = xssfWorkbook.getSheetAt( numSheet);    
            if(xssfSheet == null){    
              continue;    
            }    
                
            // 循环行Row     
            for(int rowNum = 0; rowNum <= xssfSheet.getLastRowNum(); rowNum++ ){    
              XSSFRow xssfRow = xssfSheet.getRow( rowNum);    
              if(xssfRow == null){    
                continue;    
              }    
                  
              // 循环列Cell       
              for(int cellNum = 0; cellNum <= xssfRow.getLastCellNum(); cellNum++){    
                XSSFCell xssfCell = xssfRow.getCell( cellNum);    
                if(xssfCell == null){    
                  continue;    
                }    
                KeyWord keyWord = KeyWord.dao.findFirst("select * from keyword where word = '"+ExcelUtils.getsValue(xssfCell)+"'");
                
                if(null == keyWord){
                	KeyWord keyword = new KeyWord();
                	keyword.set("id", SysUtil.getUUID());
                	keyword.set("word", ExcelUtils.getsValue(xssfCell));
                	keyword.set("enable", "0");
                	keyword.set("createdate", new Date());
                	keyword.set("updateid", admin.get("id"));
                	keyword.save();
                	
                }
              }       
            }    
          } 
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
      
      
      
      
         
	}

}
