/**
 * 
 */
package com.ideazd.system.controller;


import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.AccountedOut;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.OrderDetails;
import com.ideazd.system.model.OrderSummary;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/order",viewPath="/pages")
@Before(AdminInterceptor.class)
public class CommodityOrderController extends Controller{
	
	/**
	 * 获取商品订单列表
	 */
	public void getCommOrderList(){
		String curPageStr = getPara("curPage");
		String orderNo = getPara("orderNo");
		String orderStatus = getPara("orderStatus");
		String payDate = getPara("payDate");
		String creOrdreDate = getPara("creOrdreDate");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_order_summary s left join sta_user u on u.useid = s.useid  where ordstate != '01'";
        
        if(!StrKit.isBlank(orderNo)){
        	sql += " and s.ordernumber = '"+orderNo+"'";
        }
        
        if(!StrKit.isBlank(orderStatus)){
        	sql += " and s.ordstate = '"+orderStatus+"'";
        }
        
        if(!StrKit.isBlank(payDate)){
        	sql += " and s.paytime >'"+payDate+"' and s.paytime < "+ "'"+payDate.substring(0,10)+" 23:59:59'";
        }
        
        if(!StrKit.isBlank(creOrdreDate)){
        	sql += " and s.createtime >'"+creOrdreDate+"' and s.createtime < "+ "'"+creOrdreDate.substring(0,10)+" 23:59:59'";
        }
		
		try {
			
			Page<OrderSummary> orderSummaryPage = OrderSummary.dao.paginate(curPage, pageSize, "select s.*,u.nickname", sql+" order by s.createtime desc");
	        List<Map<Object, Object>> orderSummaryList = new ArrayList<Map<Object, Object>>();
	        
	        for(OrderSummary o:orderSummaryPage.getList()){
	        	Map<Object, Object> orderSummary = new HashMap<Object, Object>();
	        	orderSummary.put("id", o.getStr("ordid"));
	        	orderSummary.put("orderNumber",  o.get("ordernumber"));
	        	orderSummary.put("nickName",  URLDecoder.decode(o.get("nickname").toString()));
	        	orderSummary.put("ordstate",  SysUtil.getDictionValue("sta_order_summary", "ordstate", o.get("ordstate").toString()));
	        	orderSummary.put("createtime", o.get("createtime"));
	        	orderSummary.put("totalprice", o.get("totalprice"));
	        	orderSummary.put("commoditynumber", o.get("commoditynumber"));
	        	orderSummary.put("mortgageamount", o.get("mortgageamount"));
	        	orderSummary.put("paytype", SysUtil.getDictionValue("sta_order_summary", "paytype",o.get("paytype").toString()));
	        	orderSummary.put("freight", o.get("freight"));
	        	orderSummary.put("paytime", o.get("paytime"));
	        	orderSummary.put("payamount", o.get("payamount"));
	        	orderSummary.put("orderTotalPrice", new BigDecimal(o.get("totalprice").toString()).add(new BigDecimal(o.get("freight").toString())));
	        	orderSummaryList.add(orderSummary);
	        }
	        ret.put("data", orderSummaryList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(orderSummaryPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取订单详细
	 */
	public void getOrderDetail(){
		String summaryId = getPara("id");
		
		try {
			OrderSummary orderSummary = OrderSummary.dao.findById(summaryId);
			if(null != orderSummary.get("refundreson")){
				orderSummary.set("refundreson", URLDecoder.decode(orderSummary.get("refundreson").toString()));
			}
			
			setAttr("orderSummary", orderSummary);
			List<OrderDetails> orderDetailsList = OrderDetails.dao.find("select * from sta_order_details where ordid = '"+orderSummary.get("ordid")+"'");
			setAttr("orderDetailsList", orderDetailsList);
		}catch (Exception e) {
			System.out.println(e);
		}
		render("show-orderdetail.jsp");
	}
	
	/**
	 * 发货
	 */
	public void expressEdit(){
		String id = getPara("id");
		String expressnumber = getPara("expressnumber");
		String expresscompany = getPara("expresscompany");
		RetDTO ret = new RetDTO();
		
		
		
		try {
			Administrator admin = getSessionAttr("admin");
			Db.update("update sta_order_summary set expressnumber = '"+expressnumber+"',expresscompany='"+expresscompany+"',updateid='"+admin.get("id")+"',updatename='"+admin.get("name")+"',ordstate = '03',updatedate=now(),remarks='发货' where ordid='"+id+"'");
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 退货完成
	 */
	@Before(Tx.class)
	public void refundFinish(){
		String id = getPara("id");
		String outType = getPara("outType");
		String payorderId = getPara("payorderId");
		String accountName = getPara("accountName");
		String bankAccount = getPara("bankAccount");
		String cardNumber = getPara("cardNumber");
		String outRemarks = getPara("outRemarks");
		String outaMount = getPara("outaMount");
		RetDTO ret = new RetDTO();
		
		
		try {
			Administrator admin = getSessionAttr("admin");
			OrderSummary orderSummary = new OrderSummary();
			orderSummary.set("ordid", id);
			orderSummary.set("ordstate", "06");
			orderSummary.set("updateid", admin.get("id"));
			orderSummary.set("updatename", admin.get("name"));
			orderSummary.set("updatedate", new Date());
			orderSummary.set("remarks", "完成退款");
			
			if(orderSummary.update()){
				OrderSummary order = OrderSummary.dao.findById(id);
				AccountedOut accountedOut = new AccountedOut();
				accountedOut.set("accoutid", SysUtil.getUUID());
				accountedOut.set("ordid", order.get("ordid"));
				accountedOut.set("ordnumber", order.get("ordernumber"));
				accountedOut.set("useid", order.get("useid"));
				accountedOut.set("outtype", outType);
				accountedOut.set("payorderid", payorderId);
				accountedOut.set("outtime", new Date());
				accountedOut.set("outamount", outaMount);
				accountedOut.set("accountname", accountName);
				accountedOut.set("bankaccount", bankAccount);
				accountedOut.set("cardnumber", cardNumber);
				accountedOut.set("outremarks", outRemarks);
				accountedOut.set("createid", admin.get("id"));
				accountedOut.set("createname", admin.get("name"));
				accountedOut.set("createdate", new Date());
				accountedOut.set("updateid", admin.get("id"));
				accountedOut.set("updatename", admin.get("name"));
				accountedOut.set("updatedate", new Date());
				accountedOut.set("remarks", "退款出帐");
				if(accountedOut.save()){
					ret.setCode("0");
					ret.setMsg("成功");
				}
			}
			
			
			
		} catch (Exception e) {
			try {
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			ret.setCode("-1");
			ret.setMsg("系统异常！");
			
		}
		
		renderJson(ret);
		return;
	}
	
	
	

}
