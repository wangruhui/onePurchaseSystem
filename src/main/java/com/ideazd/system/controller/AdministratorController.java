/**
 * 
 */
package com.ideazd.system.controller;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.DefaultConfig;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.GetPermission;
import com.ideazd.system.model.Permission;
import com.ideazd.system.model.PermissionAssignment;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.MySessionListener;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年4月21日 下午4:00:40
 */
@ControllerBind(controllerKey="/admin")
@Before(AdminInterceptor.class)
public class AdministratorController extends Controller {
	private static final Logger log = Logger.getLogger(AdministratorController.class);
	
	
	//登录
	public void login(){
		String userName = getPara("username");
		String password = getPara("password");
		Administrator admin = Administrator.dao.findFirst("select * from administrator where account = '"+userName+"' and password='"+password+"'");
		
		if(null == admin){
			
			redirect("/login?error=false");
			//render("/pages/login.jsp");
			return;
		}
		
		if(isLogonAdmin(admin.getStr("id"))){
			redirect("/login?error=exist&ip="+admin.getStr("lastloginip"));
			//render("/pages/login.jsp");
			return;
		}
		
		
		
		/**
		 * 权限
		 */
		
		List<GetPermission> getPermissionList = new ArrayList<GetPermission>();
		
		String sql = "select * from permission where parentid is NULL";
		
		if(!admin.getStr("type").equals("1")){
			
			PermissionAssignment permissionassignment = PermissionAssignment.dao.findFirst("select * from permissionassignment where adminid = '"+admin.get("id")+"'");
			
			String perId = "''";
			
			if(permissionassignment != null){
				if(permissionassignment.getStr("permissionid").indexOf(",")>-1){
					perId = "";
					String perIds[] = permissionassignment.getStr("permissionid").split(",");
					
					for(String s:perIds){
						perId +="'"+s+"',";
					}
					perId = perId.substring(0,perId.length()-1);
				}else{
					perId = "'"+permissionassignment.getStr("permissionid")+"'";
				}
			}
			sql += " and id in ("+perId+")";
		}
		
		List<Permission> permission = Permission.dao.find(sql);
		
		for(Permission p:permission){
			GetPermission getPermission = new GetPermission();
			getPermission.setPermissionName(p.get("permissionname").toString());
			getPermission.setIoc(p.get("ioc").toString());
			List<Permission> childPermissions = Permission.dao.find("select * from permission where parentid ='"+p.get("id")+"'");
			List<Permission> childPermissionList = new ArrayList<Permission>();
			
			for(Permission cp:childPermissions){
				Permission childPermission = new Permission();
				childPermission.set("permissionname", cp.get("permissionname"));
				childPermission.set("visiturl", cp.get("visiturl"));
				childPermissionList.add(childPermission);
			}
			getPermission.setPermission(childPermissionList);
			getPermissionList.add(getPermission);
		}
		
		
		
		setSessionAttr("getPermissionList", getPermissionList);
		/**
		 * 权限
		 */
		
		
		admin.set("type",SysUtil.getDictionValue("administrator", "type", admin.getStr("type")));
		admin.set("gender",SysUtil.getDictionValue("administrator", "gender", admin.getStr("gender")));
		
		Db.update("update administrator set loginnum=loginnum+1,lastlogin=now(),lastloginip='"+getLocalIp(getRequest())+"' where id = '"+admin.getStr("id")+"'");
		System.out.println(getLocalIp(getRequest()));
		
		getSession().setAttribute("admin", admin);
		
		
		
		
		Properties props = System.getProperties();
		setSessionAttr("lastLoginDate", admin.get("lastlogin"));
		setSessionAttr("lastLoginIp", admin.get("lastloginip"));
		setSessionAttr("loginnum", admin.get("loginnum"));
		//服务器信息
		setSessionAttr("osName", props.getProperty("os.name"));
		setSessionAttr("useName", props.getProperty("user.name"));
		InetAddress addr = null;
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setSessionAttr("port", getRequest().getLocalPort());
		setSessionAttr("serverName", getRequest().getServerName());
		setSessionAttr("addr", addr.getHostAddress());
		setSessionAttr("filePath", getRequest().getSession().getServletContext().getRealPath("/"));
		setSessionAttr("cpu", Runtime.getRuntime().availableProcessors());
		
		DefaultConfig config = new DefaultConfig();
		Long functionTime = new Date().getTime() - config.startDate;
		setSessionAttr("functionTime", functionTime/1000/60);
		//setAttr("memory", Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
		
		//render("/index");
		
		
		
		redirect("/index");
	}
	
	public void adminLoginOut(){
		String outType = getPara("outType");
		getSession().removeAttribute("admin");
		
		if("wind".equals(outType)){
			RetDTO ret = new RetDTO();
			ret.setCode("0");
			ret.setMsg("成功！");
			renderJson(ret);
			return;
		}
		redirect("/login");
	}
	
	/**
	 * 添加管理员
	 */
	public void addAdmin(){
		File file = null;
		
		try {
			file = super.getFile("headportrait").getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			file = null;
		}
		String account = getPara("account");
		String name = getPara("name");
		String password = getPara("password");
		String gender = getPara("gender");
		String phone = getPara("phone");
		String email = getPara("email");
		String type = getPara("type");
		String description = getPara("description");
		String headportrait = "";
		RetDTO ret = new RetDTO();
		
		try {
			String adminId = SysUtil.getUUID();
			// 图片上传
			if (null != file) {
				
				String path = "/upload/adminHeadPortrait" + "/" + adminId + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				headportrait = imaPath.get("serverPath");

			}
			
			
			Administrator administrator = new Administrator();
			administrator.set("id", adminId);
			administrator.set("account", account);
			administrator.set("name", name);
			administrator.set("password", password);
			administrator.set("gender", gender);
			administrator.set("phone", phone);
			administrator.set("email", email);
			administrator.set("type", type);
			
			if(!StrKit.isBlank(headportrait)){
				administrator.set("headportrait", headportrait);
			}
			
			administrator.set("description", description);
			administrator.set("loginnum", 0);
			administrator.set("loginnum", 0);
			administrator.set("isenable", 0);
			administrator.set("createdate", new Date());
			
			if(administrator.save()){
				
				if(!type.equals("1")){
					Administrator admin = getSessionAttr("admin");
					PermissionAssignment pAssignment = new PermissionAssignment();
					pAssignment.set("id", SysUtil.getUUID());
					pAssignment.set("adminid", adminId);
					pAssignment.set("createid", admin.get("id"));
					pAssignment.set("createdate", new Date());
					pAssignment.set("updateid", admin.get("id"));
					pAssignment.save();
					
				}
				
				
				
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 修改管理员
	 */
	public void editAdmin(){
		File file = null;
		
		try {
			file = super.getFile("headportrait").getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			file = null;
		}
		String account = getPara("account");
		String name = getPara("name");
		String password = getPara("password");
		String gender = getPara("gender");
		String phone = getPara("phone");
		String email = getPara("email");
		String type = getPara("type");
		String description = getPara("description");
		String id = getPara("id");
		String headportrait = "";
		RetDTO ret = new RetDTO();
		
		try {
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/adminHeadPortrait" + "/" + id + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				headportrait = imaPath.get("serverPath");

			}
			
			Administrator administrator = new Administrator();
			administrator.set("id", id);
			administrator.set("account", account);
			administrator.set("name", name);
			
			if(!StrKit.isBlank(headportrait)){
				administrator.set("headportrait", headportrait);
			}
			
			administrator.set("password", password);
			administrator.set("gender", gender);
			administrator.set("phone", phone);
			administrator.set("email", email);
			administrator.set("type", type);
			administrator.set("description", description);
			
			if(!"1".equals(type)){
				PermissionAssignment p = PermissionAssignment.dao.findFirst("select * from permissionassignment where adminid = '"+id+"'");
				
				if(null == p){
					Administrator admin = getSessionAttr("admin");
					PermissionAssignment pAssignment = new PermissionAssignment();
					pAssignment.set("id", SysUtil.getUUID());
					pAssignment.set("adminid", id);
					pAssignment.set("createid", admin.get("id"));
					pAssignment.set("createdate", new Date());
					pAssignment.set("updateid", admin.get("id"));
					pAssignment.save();
				}
			}
			
			
			
			
			if(administrator.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取管理员列表
	 */
	public void getAdminList(){
		String curPageStr = getPara("curPage");
		String dateMin = getPara("datemin");
		String dateMax = getPara("datemax");
		String userName = getPara("username");
		int pageSize = 10;
		int curPage = 1;
		Map<Object,Object> ret = new HashMap<Object,Object>();
        
        if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
        
        String sql = "from administrator where 1=1";
        
        if(!StrKit.isBlank(dateMin)){
        	sql += " and createdate >= '"+dateMin+"'";
        }
        
        if(!StrKit.isBlank(dateMax)){
        	sql += " and createdate <= '"+dateMax+"'";
        }
        
        if(!StrKit.isBlank(userName)){
        	sql += " and name like '%"+userName+"%'";
        }
        
        Page<Administrator> adminPage = Administrator.dao.paginate(curPage, pageSize, "select *", sql+" order by createdate desc");
        List<Map<String, String>> adminList = new ArrayList<Map<String, String>>();
        
        for(Administrator a:adminPage.getList()){
        	Map<String, String> admin = new HashMap<String, String>();
        	admin.put("id", a.getStr("id"));
        	admin.put("account", a.getStr("account"));
        	admin.put("name", a.getStr("name"));
        	admin.put("gender", a.getStr("gender"));
        	admin.put("phone", a.getStr("phone"));
        	admin.put("email", a.getStr("email"));
        	admin.put("isenable", a.getStr("isenable"));
        	admin.put("type", SysUtil.getDictionValue("administrator", "type", a.getStr("type")));
        	admin.put("createdate", a.get("createdate").toString().substring(0,a.get("createdate").toString().length()-2));
        	adminList.add(admin);
        }
        ret.put("data", adminList);
        
        PagingUtil pagingUtil = new PagingUtil();
        pagingUtil.setRowCount(adminPage.getTotalRow());
        pagingUtil.setCurrentPage(curPage);
        
        
        ret.put("pages", pagingUtil.getPageDisplay());
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 启用禁用
	 */
	public void stopAndEnableAdmin(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			
			Administrator admin = new Administrator();
			admin.set("id", id);
			admin.set("isenable", status);
			
			if(admin.update()){
				ret.setCode("0");
				ret.setMsg("成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除管理员
	 */
	@SuppressWarnings("null")
	public void delAdmin(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from administrator where id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 根据id获取管理员信息
	 */
	public void getAdminById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		try {
			
			Administrator admin = Administrator.dao.findById(id);
			ret.put("id", admin.getStr("id"));
			ret.put("account", admin.getStr("account"));
			ret.put("name", admin.getStr("name"));
			ret.put("password", admin.getStr("password"));
			ret.put("gender", admin.getStr("gender"));
			ret.put("phone", admin.getStr("phone"));
			ret.put("email", admin.getStr("email"));
			ret.put("description", admin.getStr("description"));
			ret.put("type", admin.getStr("type"));
			ret.put("headportrait", admin.getStr("headportrait"));
			ret.put("createdate", admin.get("createdate").toString());
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
     * 从Request对象中获得客户端IP，处理了HTTP代理服务器和Nginx的反向代理截取了ip
     * @param request
     * @return ip
     */
    private String getLocalIp(HttpServletRequest request) {
       String remoteAddr = request.getRemoteAddr();
       String forwarded = request.getHeader("X-Forwarded-For");
       String realIp = request.getHeader("X-Real-IP");

       String ip = null;
       if (realIp == null) {
           if (forwarded == null) {
               ip = remoteAddr;
           } else {
               ip = remoteAddr + "/" + forwarded.split(",")[0];
           }
       } else {
           if (realIp.equals(forwarded)) {
               ip = realIp;
           } else {
               if(forwarded != null){
                   forwarded = forwarded.split(",")[0];
               }
               ip = realIp + "/" + forwarded;
           }
       }
       return ip;
   }
   
   /**
    * 判断管理员是否登录过
    * @param AdminId
    * @return
    */
   public boolean isLogonAdmin(String AdminId) {
	    Set<HttpSession> keys = MySessionListener.loginUser.keySet();
	    for (HttpSession key : keys) {
	    	/*HttpSession session = key;
	    	session.getId();*/
	        if (MySessionListener.loginUser.get(key).equals(AdminId)) {
	            return true;
	        }
	    }
	    return false;
	}
   
}
