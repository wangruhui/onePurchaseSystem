/**
 * 
 */
package com.ideazd.system.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.DataDiction;
import com.ideazd.system.model.Dictionary;
import com.ideazd.system.model.SystemConfig;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * @author yzz
 * @create 2016年4月22日 下午3:19:26
 */
@ControllerBind(controllerKey="/sys",viewPath="/pages")
@Before(AdminInterceptor.class)
public class SystemController extends Controller {
	private static final Logger log = Logger.getLogger(SystemController.class);
	
	
	public void getLoginPage(){
		render("/pages/login.jsp");
	}
	
	
	/**
	 * 分页查看字典列表
	 */
	public void getDictionList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		Map<Object,Object> ret = new HashMap<Object,Object>();
        
        if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
        
        String sql = "from dictionary order by createdate desc";
        
        Page<Dictionary> dictionPage = Dictionary.dao.paginate(curPage, pageSize, "select *", sql);
        List<Map<String, String>> dataDictionList = new ArrayList<Map<String, String>>();
        
        for(Dictionary diction:dictionPage.getList()){
        	Map<String, String> dataDiction = new HashMap<String, String>();
        	dataDiction.put("id", diction.getStr("id"));
        	dataDiction.put("dictionaryname", diction.getStr("dictionaryname"));
        	dataDiction.put("tablename", diction.getStr("tablename"));
        	dataDiction.put("fieldname", diction.getStr("fieldname"));
        	dataDiction.put("createdate", diction.get("createdate").toString());
        	dataDictionList.add(dataDiction);
        }
        ret.put("data", dataDictionList);
        
        PagingUtil pagingUtil = new PagingUtil();
        pagingUtil.setRowCount(dictionPage.getTotalRow());
        pagingUtil.setCurrentPage(curPage);
        
        
        ret.put("pages", pagingUtil.getPageDisplay());
		
		renderJson(ret);
		return;
		
	}
	
	/*
	 * 添加字典
	 */
	public void addDiction(){
		String tableName = getPara("tableName");
		String fieldName = getPara("fieldName");
		String dictionaryName = getPara("dictionaryName");
		RetDTO ret = new RetDTO();
		
		try {
			
			Dictionary dictionary = new Dictionary();
			dictionary.set("id", SysUtil.getUUID());
			dictionary.set("dictionaryname", dictionaryName);
			dictionary.set("tablename", tableName);
			dictionary.set("fieldname",fieldName );
			dictionary.set("createdate", new Date());
			
			if(dictionary.save()){
				ret.setCode("0");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 查询所有的表
	 */
	public void getTableName(){
		List<Record> tableList = Db.find("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'systemmanage'");
		StringBuffer tagJson = new StringBuffer();
		
		tagJson.append("[");
		
		for(Record r:tableList){
			tagJson.append("{")
			.append("\"tableName\":").append("\"").append(r.get("TABLE_NAME"))
	        .append("\"");
			tagJson.append("},");
			
		}
		
		
		
		String json = tagJson.substring(0,tagJson.length()-1)+"]";
		renderJson(json);
	}
	
	/**
	 * 查询表中的字段
	 */
	public void getFieldName(){
		String tableName = getPara("tableName");
		List<Record> tableList = Db.find("SELECT column_name,column_comment,data_type FROM information_schema.columns WHERE table_name='"+tableName+"'");
		StringBuffer tagJson = new StringBuffer();
		
		tagJson.append("[");
		
		for(Record r:tableList){
			tagJson.append("{")
			.append("\"fieldName\":").append("\"").append(r.get("column_name"))
	        .append("\"");
			tagJson.append("},");
			
		}
		
		
		
		String json = tagJson.substring(0,tagJson.length()-1)+"]";
		renderJson(json);
	}
	
	/**
	 * 获取系统基本配置
	 */
	public void getSystemConfig(){
		SystemConfig sc = SystemConfig.dao.findFirst("select * from systemconfig");
		RetDTO ret = new RetDTO();
		
		try {
			
			ret.put("systemname", sc.get("systemname"));
			ret.put("discription", sc.get("discription"));
			ret.put("uploadurl", sc.get("uploadurl"));
			ret.put("footright", sc.get("footright"));
			ret.put("recordnumber", sc.get("recordnumber"));
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
		
	}
	
	public void updateSystemConfig(){
		String sysName = getPara("sysName");
		String discription = getPara("discription");
		String uploadurl = getPara("uploadurl");
		String footright = getPara("footright");
		String recordnumber = getPara("recordnumber");
		RetDTO ret = new RetDTO();
		
		try {
			
			
			Db.update("update systemconfig set systemname='"+sysName+"',discription='"+discription+"',uploadurl='"+uploadurl+"',footright='"+footright+"',recordnumber='"+recordnumber+"' WHERE id = '1'");
			
			ret.setCode("0");
			ret.setMsg("修改成功");
			getSession().removeAttribute("sc");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
		
	}
	
	/**
	 * 根据表名和字段名查询字典
	 */
	public void findDictionByTablenameAndTableField(){
		String tableName = getPara("tableName");
		String tableField = getPara("tableField");
		Map<Object, Object> ret = new HashMap<Object,Object>();
		
		try {
			
			Dictionary dictionary = Dictionary.dao.findFirst("select * from dictionary where tablename = '"+tableName+"' and fieldname = '"+tableField+"'");
			
			List<DataDiction> getDictionList = DataDiction.dao.find("select * from datadiction where dictionaryid = '"+dictionary.getStr("id")+"' order by createdate ");
			List<Map<String, String>> dictionList = new ArrayList<Map<String, String>>();
			for(DataDiction d :getDictionList){
				Map<String, String> diction = new HashMap<String,String>();
				diction.put("displayvalue", d.getStr("displayvalue"));
				diction.put("fieldvalue", d.getStr("fieldvalue"));
				dictionList.add(diction);
			}
			ret.put("data",dictionList);
			ret.put("msg","成功");
			ret.put("code","0");
			
		} catch (Exception e) {
			ret.put("msg","系统异常");
			ret.put("code","-1");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 根据id查询字典
	 */
	public void findDataDictionByDictionaryId(){
		String dictionaryId = getPara("dictionaryId");
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		Map<Object,Object> ret = new HashMap<Object,Object>();
        
        if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }
		
		try {
			Page<DataDiction> dictionPage = DataDiction.dao.paginate(curPage, pageSize,"select *","from datadiction where dictionaryid = '"+dictionaryId+"' order by createdate desc");
			List<Map<String, String>> dictionList = new ArrayList<Map<String, String>>();
			for(DataDiction d :dictionPage.getList()){
				Map<String, String> diction = new HashMap<String,String>();
				diction.put("id", d.getStr("id"));
				diction.put("fieldvalue", d.getStr("fieldvalue"));
				diction.put("displayvalue", d.getStr("displayvalue"));
				diction.put("createdate", d.get("createdate").toString());
				dictionList.add(diction);
			}
			ret.put("data",dictionList);
			
			PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(dictionPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
			ret.put("msg","成功");
			ret.put("code","0");
			
		} catch (Exception e) {
			ret.put("msg","系统异常");
			ret.put("code","-1");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除字典
	 */
	@SuppressWarnings("null")
	public void delDictionary(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from dictionary where id = '"+id+"'");
				Db.update("delete from datadiction where dictionaryid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除数据字典
	 */
	@SuppressWarnings("null")
	public void delDataDictionary(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from datadiction where id = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * session是否存在
	 */
	public void seesionIsExist(){
		String sessionName = getPara("sessionName");
		
		RetDTO ret = new RetDTO();
		
		try {
			
			Object session = getSession().getAttribute(sessionName);
			
			if(null != session){
				ret.setCode("0");
				ret.setMsg("session存在");
			}else{
				ret.setCode("-2");
				ret.setMsg("session已过期");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/*
	 * 添加数据字典数据
	 */
	public void addDataDiction(){
		String fieldvalue = getPara("fieldvalue");
		String displayvalue = getPara("displayvalue");
		String dictionaryId = getPara("dictionaryId");
		System.out.println(dictionaryId);
		RetDTO ret = new RetDTO();
		
		try {
			
			DataDiction dataDictionary = new DataDiction();
			dataDictionary.set("id", SysUtil.getUUID());
			dataDictionary.set("dictionaryid", dictionaryId);
			dataDictionary.set("fieldvalue", fieldvalue);
			dataDictionary.set("displayvalue",displayvalue );
			dataDictionary.set("createdate", new Date());
			
			if(dataDictionary.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/*
	 * 编辑数据字典数据
	 */
	public void editDataDiction(){
		String fieldvalue = getPara("fieldvalue");
		String displayvalue = getPara("displayvalue");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			DataDiction dataDictionary = new DataDiction();
			dataDictionary.set("id", id);
			dataDictionary.set("fieldvalue", fieldvalue);
			dataDictionary.set("displayvalue",displayvalue );
			
			if(dataDictionary.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 根据查询字典数据详细
	 */
	public void findDataDictionById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			DataDiction dataDiction = DataDiction.dao.findById(id);
			ret.put("displayvalue", dataDiction.getStr("displayvalue"));
			ret.put("fieldvalue", dataDiction.getStr("fieldvalue"));
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常");
		}
		
		renderJson(ret);
		return;
		
		
	}
	
	/**
	 * 查询值是否存在
	 */
	public void findDataDictionByFieldValue(){
		String dictionaryId = getPara("dictionaryId");
		String fieldValue = getPara("fieldValue");
		String datadictionId = getPara("datadictionId");
		RetDTO ret = new RetDTO();
		
		try {
			String sql = "select * from datadiction where dictionaryid = '"+dictionaryId+"' and fieldvalue = '"+fieldValue+"'";
			
			if(!StrKit.isBlank(datadictionId)){
				sql += " and id != '"+datadictionId+"'";
			}
			
			DataDiction dataDictionary = DataDiction.dao.findFirst(sql);
			
			if(null == dataDictionary){
				ret.setCode("0");
				ret.setMsg("可以添加");
			}else{
				ret.setCode("-2");
				ret.setMsg("该值已存在");
			}

			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
}
