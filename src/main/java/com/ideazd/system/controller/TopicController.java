/**
 * 
 */
package com.ideazd.system.controller;

import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ideazd.common.utils.PagingUtil;
import com.ideazd.system.interceptor.AdminInterceptor;
import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.NewDetails;
import com.ideazd.system.model.Topic;
import com.ideazd.system.model.TopicComment;
import com.ideazd.system.tools.DateUtil;
import com.ideazd.system.tools.SysUtil;
import com.ideazd.system.util.RetDTO;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author yzz
 * @create 2016年5月5日 下午3:12:46
 */
@ControllerBind(controllerKey="/topic",viewPath="/pages")
@Before(AdminInterceptor.class)
public class TopicController extends Controller{
	
	/**
	 * 获取话题列表
	 */
	public void getTopicList(){
		String curPageStr = getPara("curPage");
		String title = getPara("title");
		String publishDate = getPara("publishDate");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_topic c left join sta_user u on c.useid = u.useid where 1 = 1";
        
        if(!StrKit.isBlank(title)){
        	sql += " and c.toptitle like '%"+title+"%'";
        }
        
        if(!StrKit.isBlank(publishDate)){
        	sql += " and c.createtime >'"+publishDate+"' and c.createtime < "+ "'"+publishDate.substring(0,10)+" 23:59:59'";
        }
		
		try {
			
			Page<Topic> topicPage = Topic.dao.paginate(curPage, pageSize, "select c.*,u.nickname", sql+" order by c.updatedate desc");
	        List<Map<String, String>> topictList = new ArrayList<Map<String, String>>();
	        
	        for(Topic t:topicPage.getList()){
	        	Map<String, String> topic = new HashMap<String, String>();
	        	topic.put("id", t.getStr("topid"));
	        	topic.put("nickName", t.getStr("nickname"));
	        	topic.put("toptitle",  URLDecoder.decode((t.getStr("toptitle"))));
	        	topic.put("createtime", t.get("createtime").toString());
	        	topic.put("displayorder", t.get("displayorder").toString());
	        	topic.put("displaystatus", t.get("displaystatus").toString());
	        	topic.put("readamount", t.get("readamount").toString());
	        	topic.put("clickamount", t.get("clickamount").toString());
	        	topic.put("toppicture", t.get("toppicture").toString());
	        	topic.put("createname", t.getStr("createname"));
	        	topic.put("updatename", t.getStr("updatename"));
	        	topictList.add(topic);
	        }
	        ret.put("data", topictList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(topicPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 添加话题
	 */
	public void addTopic(){
		File file = super.getFile("toppicture").getFile();
		//String ss = super.getFile("detpicture").getFileName();
		String toptitle = getPara("toptitle");
		//UploadFile upFile = getFile(getPara("detpicture"),"e:/");
		String topcontent = getPara("topcontent");
		String createtime = getPara("createtime");
		String displayorder = getPara("displayorder");
		String displaystatus = getPara("displaystatus");
		RetDTO ret = new RetDTO();
		String toppicture = "";
		
		
		
		try {
			
			String uuid = SysUtil.getUUID();
			
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/newsdetail" + "/" + uuid + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				toppicture = imaPath.get("serverPath");

			}
			
			
			
			Administrator admin = getSessionAttr("admin");
			
			Topic topic = new Topic();
			topic.set("topid", uuid);
			topic.set("toptitle", toptitle);
			topic.set("toppicture", toppicture);
			topic.set("useid", "administrator");
			topic.set("topcontent", topcontent);
			
			if(StrKit.isBlank(createtime)){
				topic.set("createtime", new Date());
			}else{
				topic.set("createtime", createtime);
			}
			
			topic.set("displayorder", displayorder);
			topic.set("displaystatus", displaystatus);
			topic.set("readamount", 0);
			topic.set("clickamount", 0);
			topic.set("commentamount", "0");
			topic.set("createid", admin.get("id"));
			topic.set("createname", admin.get("name"));
			topic.set("createdate", new Date());
			topic.set("updateid", admin.get("id"));
			topic.set("updatename", admin.get("name"));
			topic.set("updatedate", new Date());
			topic.set("remarks", "添加话题");
			
			if(topic.save()){
				ret.setCode("0");
				ret.setMsg("添加成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 编辑话题
	 */
	public void editTopic(){
		File file = null;
		
		try {
			file = super.getFile("toppicture").getFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			file = null;
		}
		//String ss = super.getFile("detpicture").getFileName();
		String toptitle = getPara("toptitle");
		//UploadFile upFile = getFile(getPara("detpicture"),"e:/");
		String topcontent = getPara("topcontent");
		String createtime = getPara("createtime");
		String displayorder = getPara("displayorder");
		String displaystatus = getPara("displaystatus");
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		String toppicture = "";
		
		
		
		try {
			
			// 图片上传
			if (null != file) {
				
				String path = "/upload/newsdetail" + "/" + id + "/images" + "/"+ DateUtil.formatDate(new Date(), "yyyyMMdd") + "/"; 

				Map<String, String> imaPath = SysUtil.uploadImg(getRequest(),file,path);

				toppicture = imaPath.get("serverPath");

			}
			
			
			
			Administrator admin = getSessionAttr("admin");
			
			Topic topic = new Topic();
			topic.set("topid", id);
			topic.set("toptitle", toptitle);
			
			if(!StrKit.isBlank(toppicture)){
				topic.set("toppicture", toppicture);
			}
			
			
			topic.set("topcontent", topcontent);
			
			if(StrKit.isBlank(createtime)){
				topic.set("createtime", new Date());
			}else{
				topic.set("createtime", createtime);
			}
			
			topic.set("displayorder", displayorder);
			topic.set("displaystatus", displaystatus);
			topic.set("updateid", admin.get("id"));
			topic.set("updatename", admin.get("name"));
			topic.set("updatedate", new Date());
			topic.set("remarks", "修改话题");
			
			if(topic.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 删除话题
	 */
	@SuppressWarnings("null")
	public void delTopic(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("delete from sta_topic where topid = '"+id+"'");
				Db.update("delete from sta_topic_comment where topid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 根据id获取话题
	 */
	public void findTopicById(){
		String id = getPara("id");
		RetDTO ret = new RetDTO();
		
		try {
			
			
			Topic t = Topic.dao.findById(id);
			ret.put("id", t.getStr("topid"));
			ret.put("toptitle",  URLDecoder.decode(t.getStr("toptitle")));
        	ret.put("toppicture", t.getStr("toppicture"));
        	ret.put("topcontent", URLDecoder.decode(t.getStr("topcontent")));
        	ret.put("createtime", t.get("createtime").toString());
        	ret.put("displayorder", t.get("displayorder").toString());
        	ret.put("displaystatus", t.get("displaystatus").toString());
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 启用/禁用
	 */
	public void handleDisplay(){
		String id = getPara("id");
		String status = getPara("status");
		RetDTO ret = new RetDTO();
		
		try {
			Administrator admin = getSessionAttr("admin");
			
			Topic topic = new Topic();
			topic.set("topid", id);
			topic.set("displaystatus", status);
			topic.set("updateid", admin.get("id"));
			topic.set("updatename", admin.get("name"));
			topic.set("updatedate", new Date());
			
			if(topic.update()){
				ret.setCode("0");
				ret.setMsg("修改成功");
			}
			
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取话题评论列表
	 */
	public void getTopicCommentList(){
		String curPageStr = getPara("curPage");
		String topicId = getPara("topicId");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_topic_comment c left join sta_user u on c.useid = u.useid  where c.topid = '"+topicId+"' and c.delflag = '0'";
		
		try {
			
			Page<TopicComment> topicCommentPage = TopicComment.dao.paginate(curPage, pageSize, "select c.*,u.nickname", sql+" order by c.createtime desc");
	        List<Map<String, String>> topicCommentList = new ArrayList<Map<String, String>>();
	        
	        for(TopicComment t:topicCommentPage.getList()){
	        	Map<String, String> topicComment = new HashMap<String, String>();
	        	topicComment.put("id", t.getStr("comid"));
	        	topicComment.put("comcontent",  URLDecoder.decode(t.getStr("comcontent").toString()));
	        	topicComment.put("nickname", URLDecoder.decode(t.getStr("nickname")));
	        	topicComment.put("createtime", t.get("createtime").toString());
	        	topicCommentList.add(topicComment);
	        }
	        ret.put("data", topicCommentList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(topicCommentPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}
	
	/**
	 * 获取评论列表
	 */
	public void getCommentList(){
		String curPageStr = getPara("curPage");
		int pageSize = 10;
		int curPage = 1;
		
		Map<Object,Object> ret = new HashMap<Object,Object>();
		
		if(!StrKit.isBlank(curPageStr)){
        	curPage = Integer.parseInt(curPageStr);
        }

		String sql = "from sta_topic_comment c left join sta_user u on c.useid = u.useid left join sta_topic t on t.topid = c.topid where c.delflag = '0'";
		
		try {
			
			Page<TopicComment> topicCommentPage = TopicComment.dao.paginate(curPage, pageSize, "select c.*,u.nickname,t.toptitle", sql+" order by c.createtime desc");
	        List<Map<String, String>> topicCommentList = new ArrayList<Map<String, String>>();
	        
	        for(TopicComment t:topicCommentPage.getList()){
	        	Map<String, String> topicComment = new HashMap<String, String>();
	        	topicComment.put("id", t.getStr("comid"));
	        	topicComment.put("toptitle",  URLDecoder.decode(t.getStr("toptitle").toString()));
	        	topicComment.put("comcontent",  URLDecoder.decode(t.getStr("comcontent").toString()));
	        	topicComment.put("nickname", URLDecoder.decode(t.getStr("nickname")));
	        	topicComment.put("createtime", t.get("createtime").toString());
	        	topicCommentList.add(topicComment);
	        }
	        ret.put("data", topicCommentList);
	        
	        PagingUtil pagingUtil = new PagingUtil();
	        pagingUtil.setRowCount(topicCommentPage.getTotalRow());
	        pagingUtil.setCurrentPage(curPage);
	        
	        
	        ret.put("pages", pagingUtil.getPageDisplay());
	        ret.put("code", "0");
	        ret.put("msg", "成功");
			
		}catch (Exception e) {
			ret.put("code", "-1");
	        ret.put("msg", "系统异常");
		}
		
		renderJson(ret);
		return;
	}

	
	/**
	 * 删除话题
	 */
	@SuppressWarnings("null")
	public void delTopicComment(){
		String ids = getPara("ids");
		String idArray[] = null;
		RetDTO ret = new RetDTO();
		
		if(ids.indexOf(",") < -1){
			idArray[0] = ids;
		}else{
			idArray = ids.split(",");
		}
		
		try {
			
			for(String id:idArray){
				Db.update("update  sta_topic_comment set delflag = '1' where comid = '"+id+"'");
			}
			
			ret.setCode("0");
			ret.setMsg("成功");
			
		} catch (Exception e) {
			ret.setCode("-1");
			ret.setMsg("系统异常！");
		}
		
		renderJson(ret);
		return;
	}
	

}
