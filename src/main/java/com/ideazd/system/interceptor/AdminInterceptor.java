package com.ideazd.system.interceptor;


import com.ideazd.system.model.Administrator;
import com.ideazd.system.model.SystemConfig;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * 拦截器
 * @author yzz
 *
 */

public class AdminInterceptor implements Interceptor {
	
	public void intercept(Invocation ai) {
		Controller controller = ai.getController();
		String serverURL = controller.getRequest().getServletPath();
		if(serverURL.indexOf("show") > -1){
			ai.invoke();
			return;
		}
		
		
		Administrator admin = controller.getSessionAttr("admin");
		SystemConfig sc = controller.getSessionAttr("sc");
		
		//基本配置
		if(null == sc){
			 sc = SystemConfig.dao.findFirst("select * from systemconfig");
			 controller.setSessionAttr("sc", sc);
		}
		
		if(null == admin){
			
			
			String ajax = ai.getController().getRequest().getHeader("X-Requested-With");

			if("XMLHttpRequest".equals(ajax)){
				String ret = "{\"code\":\"-3\"}";
				controller.renderJson(ret);
				return;
			}
			
			if(serverURL.indexOf("/login") > -1){
				ai.invoke();
				//controller.redirect("/pages/index.jsp");
				return;
			}
			controller.redirect("/login");
		}else{
			if(serverURL.indexOf("/login") > -1){
				controller.redirect("/index");
				return;
			}
			
			ai.invoke();
		    System.out.println("After invoking " + ai.getActionKey());
		}
	}
}
